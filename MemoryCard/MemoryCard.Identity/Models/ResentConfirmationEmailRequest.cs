﻿namespace MemoryCard.Identity.Models;

public sealed record ResentConfirmationEmailRequest(string EmailAddress);
