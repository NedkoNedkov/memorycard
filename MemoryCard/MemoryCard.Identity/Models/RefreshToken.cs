﻿namespace MemoryCard.Identity.Models;

public sealed record RefreshToken(string Token, DateTime Expire);
