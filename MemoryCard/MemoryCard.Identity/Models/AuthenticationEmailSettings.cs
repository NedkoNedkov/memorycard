﻿namespace MemoryCard.Identity.Models;

public class AuthenticationEmailSettings
{
    public string From { get; set; } = string.Empty;
    public string ApplicationName { get; set; } = string.Empty;
    public string ConfirmEmailPath { get; set; } = string.Empty;
    public string ResetPasswordPath { get; set; } = string.Empty;
}
