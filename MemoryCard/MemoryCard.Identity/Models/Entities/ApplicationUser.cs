﻿using MemoryCard.Domain.ValueObjects;
using Microsoft.AspNetCore.Identity;

namespace MemoryCard.Identity.Models.Entities;

public class ApplicationUser : IdentityUser
{
    public FirstName FirstName { get; set; }
    public LastName LastName { get; set; }
    public string? RefreshToken { get; set; } = null;
    public DateTime RefreshTokenExpireTime { get; set; }
    public bool IsRefrshTokenExpired => DateTime.UtcNow >= RefreshTokenExpireTime;
    public string GetFullName() => $"{FirstName.Value} {LastName.Value}";
}
