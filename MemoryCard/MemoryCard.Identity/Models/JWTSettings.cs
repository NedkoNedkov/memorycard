﻿namespace MemoryCard.Identity.Models;

public class JWTSettings
{
    public string Key { get; set; } = string.Empty;
    public string Issuer { get; set; } = string.Empty;
    public string Audience { get; set; } = string.Empty;
    public string AppUrl {  get; set; } = string.Empty;
    public int DurationInMinutes { get; set; }
    public int RefreshTokenDurationInHours { get; set; }
}
