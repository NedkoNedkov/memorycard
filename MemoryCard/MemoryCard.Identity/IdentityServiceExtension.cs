﻿using FluentValidation;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Data;
using MemoryCard.Identity.Models;
using MemoryCard.Identity.Models.Entities;
using MemoryCard.Identity.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace MemoryCard.Identity;

public static class IdentityServiceExtension
{
    public static IServiceCollection AddIdentityServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<JWTSettings>(configuration.GetSection("JwtSettings"));
        services.Configure<AuthenticationEmailSettings>(configuration.GetSection("AuthenticationEmailSettings"));

        services.AddDbContext<MemoryCardIdentityDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("MemoryCradsIdentityConnectionString"),
            b => b.MigrationsAssembly(typeof(MemoryCardIdentityDbContext).Assembly.FullName)));

        services.AddIdentity<ApplicationUser, IdentityRole>(options =>
        {
            options.Password.RequiredLength = 8;
            options.Password.RequireDigit = false;
            options.Password.RequireLowercase = false;
            options.Password.RequireUppercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.SignIn.RequireConfirmedEmail = true;
        })
            .AddEntityFrameworkStores<MemoryCardIdentityDbContext>()
            .AddRoleManager<RoleManager<IdentityRole>>()
            .AddSignInManager<SignInManager<ApplicationUser>>()
            .AddUserManager<UserManager<ApplicationUser>>()
            .AddDefaultTokenProviders();
                
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;
                o.SaveToken = false;
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero,
                    ValidIssuer = configuration["JwtSettings:Issuer"],
                    ValidAudience = configuration["JwtSettings:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSettings:Key"]))
                };                
            });

        services.AddAuthorization(options =>
        {
            options.AddPolicy("AdminPolicy", policy => policy.RequireRole("Admin"));
            options.AddPolicy("ManagerPolicy", policy => policy.RequireRole("Manager"));
            options.AddPolicy("PlayerPolicy", policy => policy.RequireRole("Player"));

            options.AddPolicy("AdminOrManagerPolicy", policy => policy.RequireRole("Admin", "Manager"));
            options.AddPolicy("AdminAndManagerPolicy", policy => policy.RequireRole("Admin").RequireRole("Manager"));
            options.AddPolicy("AllRolePolicy", policy => policy.RequireRole("Admin", "Manager", "Player"));
        });

        services.AddScoped<ITokenProvider, TokenProvider>();        
        services.AddScoped<IAuthenticationService, AuthenticationService>();
        services.AddValidatorsFromAssembly(typeof(IdentityServiceExtension).Assembly, includeInternalTypes: true);
        services.AddScoped<IAccountEmailService, AccountEmailService>();
        

        return services;
    }
}
