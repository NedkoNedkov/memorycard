﻿using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MemoryCard.Identity.Data;

public class MemoryCardIdentityDbContext : IdentityDbContext<ApplicationUser>
{
    public MemoryCardIdentityDbContext(DbContextOptions<MemoryCardIdentityDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(typeof(MemoryCardIdentityDbContext).Assembly);
        base.OnModelCreating(builder);
    }
}
