﻿using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace MemoryCard.Identity.Services;

public class TokenProvider : ITokenProvider
{
    private readonly UserManager<ApplicationUser> userManager;
    private readonly JWTSettings jwtSettings;

    public TokenProvider(UserManager<ApplicationUser> userManeger, IOptions<JWTSettings> jwtSettings)
    {
        userManager = userManeger;
        this.jwtSettings = jwtSettings.Value;
    }
    public async Task<string> CreateAuthenticationTokenAsync(ApplicationUser user)
    {
        var userClaims = await userManager.GetClaimsAsync(user);
        var roles = await userManager.GetRolesAsync(user);

        var roleClaims = new List<Claim>();

        foreach (var role in roles)
        {
            roleClaims.Add(new Claim("roles", role));
        }

        var claims = new[]
        {
            new Claim(JwtRegisteredClaimNames.Sub, user.GetFullName()),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new Claim(JwtRegisteredClaimNames.Email, user.Email),
            new Claim("FirstName",user.Email),
            new Claim("LastName",user.LastName.Value)
        }
        .Union(userClaims)
        .Union(roleClaims);

        var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key));
        var signinCredentials = new SigningCredentials(symetricSecurityKey, SecurityAlgorithms.HmacSha256);
        var authenticationToken = new JwtSecurityToken(
            issuer: jwtSettings.Issuer,
            audience: jwtSettings.Audience,
            claims: claims,
            expires: DateTime.UtcNow.AddMinutes(jwtSettings.DurationInMinutes),
            signingCredentials: signinCredentials);

        var token = new JwtSecurityTokenHandler().WriteToken(authenticationToken);

        return token;
    }

    public async Task<Result<RefreshToken>> CreateRefreshTokenAsync(ApplicationUser user)
    {
        var token = new byte[32];
        using var randomNumberGenerator = RandomNumberGenerator.Create();
        randomNumberGenerator.GetBytes(token);

        var refreshToken = Convert.ToBase64String(token);

        user.RefreshToken = refreshToken;
        user.RefreshTokenExpireTime = DateTime.UtcNow.AddHours(jwtSettings.RefreshTokenDurationInHours);

        var result = await userManager.UpdateAsync(user);

        if (!result.Succeeded)
        {
            return Result.Failure<RefreshToken>(DomainErrors.ApplicationUser.InvalidCredentials);
        }
       
        return new RefreshToken(refreshToken,user.RefreshTokenExpireTime);
    }

    public Result<string> GetUserEmailAddressFromExpiredToken(string token)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            ValidateIssuer = true,
            ValidateAudience = false,
            ValidateLifetime = false,
            ClockSkew = TimeSpan.Zero,
            ValidIssuer = jwtSettings.Issuer,
            ValidAudience = jwtSettings.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key))
        };

        var tokenHandler = new JwtSecurityTokenHandler();

        SecurityToken securityToken;

        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);

        var jwtSecurityToken = securityToken as JwtSecurityToken;

        if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
        {
            return Result.Failure<string>(DomainErrors.ApplicationUser.InvalidCredentials);
        }

        return principal?.FindFirst(ClaimTypes.Email)?.Value;
    }

}
