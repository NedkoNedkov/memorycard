﻿using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Constants;
using MemoryCard.Application.Models.Identity.Registration;
using MemoryCard.Domain.ValueObjects;
using MemoryCard.Application.Models.Identity.AccountConfirmation;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;
using MemoryCard.Application.Models.Identity.RefreshToken;
using MemoryCard.Application.Models.Identity.ResetPassword;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Domain.Errors;
using MemoryCard.Identity.Utility;
using MemoryCard.Identity.Services;
using MemoryCard.Identity.Utility.Authentication;

namespace MemoryCard.Identity.Services;

public class AuthenticationService : IAuthenticationService
{
    private readonly UserManager<ApplicationUser> userManager;
    private readonly IAccountEmailService accountEmailService;
    private readonly SignInManager<ApplicationUser> signInManager;
    private readonly ITokenProvider tokenProvider;

    public AuthenticationService(
        UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager,
        ITokenProvider tokenProvider,
        IAccountEmailService accountEmailService)
    {
        this.userManager = userManager;
        this.signInManager = signInManager;
        this.tokenProvider = tokenProvider;
        this.accountEmailService = accountEmailService;
    }

    #region Authentication
    public async Task<Result<AuthenticationResponse>> LoginAsync(AuthenticationRequest request)
    {
        var result = await Login.LoginAsync(userManager, signInManager, tokenProvider, request);

        return result;
    }
    #endregion

    #region Registration
    public async Task<Result<RegistrationResponse>> RegistrationAsync(RegistrationRequest request)
    {
        var result = await Registration.RegistrationAsync(userManager, accountEmailService, request);

        return result;
    }
    #endregion

    #region Account confirmation
    public async Task<Result> AccountConfirmationAsync(AccountConfirmationRequest request)
    {
        var result = await AccountConfirmation.AccountConfirmationAsync(userManager, request);

        return result;
    }

    public async Task<Result> ResendConfirmationEmailAsync(EmailAddress emailAddress)
    {
        var user = await userManager
            .EmailAddressIsConfirmedAsync(emailAddress.Value, false);

        if (user.IsFailure)
        {
            return Result.Failure(user.Error);
        }

        var result = await accountEmailService.SendConfirmationEmailAsync(user.Value);

        return result;
    }
    #endregion

    #region RefreshToken
    public async Task<Result<AuthenticationResponse>> RefreshTokenAsync(RefreshTokenRequest request)
    {
        var result = await RefreshToken.RefreshTokenAsync(userManager,tokenProvider, request);

        return result;
    }
    #endregion

    #region Forgot password
    public async Task<Result> ForgotPasswordAsync(EmailAddress emailAddress)
    {
        var user = await userManager
            .EmailAddressIsConfirmedAsync(emailAddress.Value, true);

        if (user.IsFailure)
        {
            return Result.Failure(user.Error);
        }

        var resetToken = await userManager.GeneratePasswordResetTokenAsync(user.Value);

        resetToken = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(resetToken));

        var result = await accountEmailService.SendResetPasswordEmailAsync(user.Value);

        return result;
    }

    public async Task<Result> ResetPasswordAsync(ResetPasswordRequest request)
    {
        var user = await userManager
            .EmailAddressIsConfirmedAsync(request.EmailAddress, true);

        if (user.IsFailure)
        {
            return Result.Failure(user.Error);
        }

        if (!request.Password.Equals(request.ConfirmPassword))
        {
            return Result.Failure(DomainErrors.ApplicationUser.ConfirmPasswordNotMatch);
        }

        var decodedTokenBytes = WebEncoders.Base64UrlDecode(request.ResetToken);

        var decodedToken = Encoding.UTF8.GetString(decodedTokenBytes);

        var result = await userManager.ResetPasswordAsync(user.Value, decodedToken, request.Password);

        if (!result.Succeeded)
        {
            return Result.Failure(DomainErrors.ApplicationUser.PasswordResetFailed);
        }

        return Result.Success();
    }
    #endregion


}
