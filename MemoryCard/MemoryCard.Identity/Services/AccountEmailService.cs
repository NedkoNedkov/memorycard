﻿using MemoryCard.Application.Contracts.Infrastructure;
using MemoryCard.Application.Models.Mail;
using MemoryCard.Application.Models.UserModels;
using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Domain.ValueObjects;
using MemoryCard.Identity.Constants;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using System.Text;

namespace MemoryCard.Identity.Services;

public class AccountEmailService : IAccountEmailService
{
    public const string EmailConfirmationSubject = "Account confirmation";
    public const string ResetPassordSubject = "Password resetting";

    private readonly JWTSettings jwtSettings;
    private readonly AuthenticationEmailSettings authenticationEmailSettings;
    private readonly UserManager<ApplicationUser> userManager;
    private readonly IEmailService emailService;
    private readonly ITokenProvider tokenProvider;

    public AccountEmailService(
        UserManager<ApplicationUser> userManager,
        IEmailService emailService,
        IOptions<JWTSettings> jwtSettings,
        IOptions<AuthenticationEmailSettings> authenticationEmailSettings)
    {
        this.jwtSettings = jwtSettings.Value;
        this.authenticationEmailSettings = authenticationEmailSettings.Value;
        this.userManager = userManager;
        this.emailService = emailService;
    }

    public async Task<Result> SendConfirmationEmailAsync(ApplicationUser user)
    {
        var confirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(user);

        confirmationToken = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(confirmationToken));

        var emailBody = CreateConfirmationEmailBody(user.Email, confirmationToken);

        var email = CreateEmailForm(user, emailBody);

        var result = await emailService.SendEmailAsync(email);

        if (result.IsFailure)
        {
            return Result.Failure(result.Error);
        }

        return Result.Success();
    }

    public async Task<Result> SendResetPasswordEmailAsync(ApplicationUser user)
    {
        if (!user.EmailConfirmed)
        {
            return Result.Failure(DomainErrors.ApplicationUser.EmailAddressNotConfirm);
        }

        var resetToken = await this.userManager.GeneratePasswordResetTokenAsync(user);

        resetToken = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(resetToken));

        var emailBody = CreateResetPasswordEmailBody(user.Email, user.FirstName.Value, resetToken);

        var email = CreateEmailForm(user, emailBody);

        var result = await this.emailService.SendEmailAsync(email);

        if (result.IsFailure)
        {
            return Result.Failure(result.Error);
        }

        return Result.Success();
    }

    private string CreateConfirmationEmailBody(string emailAddress, string confirmationToken)
    {
        string confirmationLink = $"{jwtSettings.AppUrl}/{authenticationEmailSettings.ConfirmEmailPath}?emailaddress={emailAddress}&confirmationToken={confirmationToken}";

        var body = AccountEmailTamplates.GetConfirmationEmailTamplate(authenticationEmailSettings.ApplicationName, confirmationLink, authenticationEmailSettings.From);

        return body;
    }

    private string CreateResetPasswordEmailBody(string emailAddress, string firstName, string resetToken)
    {
        string resetLink = $"{jwtSettings.AppUrl}/{authenticationEmailSettings.ResetPasswordPath}?emailaddress={emailAddress}&token={resetToken}";

        var body = AccountEmailTamplates.GetResetEmail(firstName, resetLink, authenticationEmailSettings.From);

        return body;
    }

    private EmailForm CreateEmailForm(ApplicationUser recipient, string body)
    {
        var emailForm = new EmailForm
        (            
            new RegularMember
            (
                EmailAddress.Create(authenticationEmailSettings.From).Value,
                authenticationEmailSettings.ApplicationName
            ),
            new List<RegularMember>
            {
                new RegularMember
                (
                    EmailAddress.Create(recipient.Email).Value,
                    recipient.GetFullName()
                )
            },
            EmailConfirmationSubject,
            body
        );

        return emailForm;
    }
}
