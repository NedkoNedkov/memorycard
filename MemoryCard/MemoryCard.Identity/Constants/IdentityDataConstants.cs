﻿namespace MemoryCard.Identity.Constants;

public static class IdentityDataConstants
{
    public const string Facebook = "facebook";
    public const string Google = "google";

    public const string AdminRole = "Admin";
    public const string ManagerRole = "Manager";
    public const string PlayerRole = "Player";

    public const string AdminUserName = "ngnedkov80@gmail.com";
    public const string SuperAdminChangeNotAllowed = "Super Admin change is not allowed!";
    public const int MaximumLoginAttempts = 3;

    public const int LockedPeriodInMinutes = 10;
}
