﻿using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Models;
using MemoryCard.Identity.Models.Entities;

namespace MemoryCard.Identity.Contracts;

public interface ITokenProvider
{
    public Task<string> CreateAuthenticationTokenAsync(ApplicationUser user);
    public Task<Result<RefreshToken>> CreateRefreshTokenAsync(ApplicationUser user);
    Result<string> GetUserEmailAddressFromExpiredToken(string token);
}
