﻿using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Models.Entities;

namespace MemoryCard.Identity.Contracts;

public interface IAccountEmailService
{
    public Task<Result> SendConfirmationEmailAsync(ApplicationUser user);
    public Task<Result> SendResetPasswordEmailAsync(ApplicationUser user);
}
