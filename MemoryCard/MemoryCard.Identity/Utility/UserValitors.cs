﻿using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace MemoryCard.Identity.Utility;

public static class UserValitors
{
    public static async Task<Result<ApplicationUser>> ExistingUserAsync(this UserManager<ApplicationUser> userManager, string emailAddress)
    {
        var user = await userManager.FindByEmailAsync(emailAddress);

        if (user == null)
        {
            return Result.Failure<ApplicationUser>(DomainErrors.ApplicationUser.NotFound(emailAddress));
        }

        return user;
    }

    public static async Task<Result> NoExistingUserAsync(this UserManager<ApplicationUser> userManager, string emailAddress)
    {
        var user = await userManager.FindByEmailAsync(emailAddress);

        if (user != null)
        {
            return Result.Failure<ApplicationUser>(DomainErrors.ApplicationUser.EmailAlreadyInUse);
        }

        return Result.Success();
    }

    public static async Task<Result<ApplicationUser>> EmailAddressIsConfirmedAsync(this UserManager<ApplicationUser> userManager, string emailAddress, bool isConfirmed)
    {
        Result<ApplicationUser> result = await ExistingUserAsync(userManager, emailAddress);

        if (result.IsFailure)
        {
            return result;
        }

        if (isConfirmed)
        {
            result = (isConfirmed == result.Value.EmailConfirmed) ? result.Value : Result.Failure<ApplicationUser>(DomainErrors.ApplicationUser.EmailAddressNotConfirm);
            return result;
        }

        result = (isConfirmed == result.Value.EmailConfirmed) ? result.Value : Result.Failure<ApplicationUser>(DomainErrors.ApplicationUser.EmailAlreadyInUse);
        return result;
    }
}
