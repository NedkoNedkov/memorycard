﻿//using MemoryCard.Identity.Contracts;
//using MemoryCard.Identity.Models.Entities;
//using Microsoft.AspNetCore.Identity;
//using MemoryCard.Application.Models.Identity.Authentication;
//using MemoryCard.Domain.Shared;
//using MemoryCard.Identity.Constants;
//using MemoryCard.Application.Models.Identity.Registration;
//using MemoryCard.Domain.ValueObjects;
//using MemoryCard.Application.Models.Identity.AccountConfirmation;
//using Microsoft.AspNetCore.WebUtilities;
//using System.Text;
//using MemoryCard.Application.Models.Identity.RefreshToken;
//using MemoryCard.Application.Models.Identity.ResetPassword;
//using MemoryCard.Application.Contracts.Identity;
//using MemoryCard.Domain.Errors;
//using MemoryCard.Identity.Utility;
//using System.Net.Mail;

//namespace MemoryCard.Identity.Services;

//public class AuthenticationService : IAuthenticationService
//{
//    private readonly UserManager<ApplicationUser> userManager;
//    private readonly IAccountEmailService accountEmailervice;
//    private readonly SignInManager<ApplicationUser> signInManager;
//    private readonly ITokenProvider tokenProvider;

//    public AuthenticationService(
//        UserManager<ApplicationUser> userManager,
//        SignInManager<ApplicationUser> signInManager,
//        ITokenProvider tokenProvider,
//        IAccountEmailService accountEmailservice)
//    {
//        this.userManager = userManager;
//        this.signInManager = signInManager;
//        this.tokenProvider = tokenProvider;
//        accountEmailervice = accountEmailservice;
//    }

//    #region Authentication
//    public async Task<Result<AuthenticationResponse>> LoginAsync(AuthenticationRequest request)
//    {
//        var user = (await userManager.ExistingUserAsync(request.EmailAddress))
//            .EmailAddressIsConfirmed(true);

//        if (user.IsFailure)
//        {
//            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
//        }

//        var result = await signInManager.PasswordSignInAsync(user.Value.Email, request.Password, false, lockoutOnFailure: false);

//        if (result.IsLockedOut)
//        {
//            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.AccountIsLocked(RemainingTime.Time(user.Value.LockoutEnd.Value)));
//        }

//        if (!result.Succeeded)
//        {
//            if (!user.Value.UserName.Equals(IdentityDataConstants.AdminUserName))
//            {
//                await userManager.AccessFailedAsync(user.Value);
//            }

//            if (user.Value.AccessFailedCount >= IdentityDataConstants.MaximumLoginAttempts)
//            {
//                await userManager.SetLockoutEndDateAsync(user.Value, DateTime.UtcNow.AddMinutes(IdentityDataConstants.LockedPeriodInMinutes));
//                return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.AccountIsLocked(RemainingTime.Time(user.Value.LockoutEnd.Value)));
//            }

//            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
//        }

//        var authenticationToken = await tokenProvider.CreateAuthenticationTokenAsync(user.Value);


//        var refreshTokenResult = await tokenProvider.CreateRefreshTokenAsync(user.Value);

//        if (refreshTokenResult.IsFailure)
//        {
//            return Result.Failure<AuthenticationResponse>(refreshTokenResult.Error);
//        }

//        var response = new AuthenticationResponse
//        (
//            authenticationToken,
//            refreshTokenResult.Value.Token,
//            refreshTokenResult.Value.Expire
//        );

//        return response;
//    }
//    #endregion

//    #region Registration
//    public async Task<Result<RegistrationResponse>> RegistrationAsync(RegistrationRequest request)
//    {
//        var existingUser = await userManager.NoExistingUserAsync(request.EmailAddress);

//        if (existingUser.IsFailure)
//        {
//            return Result.Failure<RegistrationResponse>(existingUser.Error);
//        }

//        if (!request.Password.Equals(request.ConfirmPassword))
//        {
//            return Result.Failure<RegistrationResponse>(DomainErrors.ApplicationUser.ConfirmPasswordNotMatch);
//        }

//        var user = new ApplicationUser
//        {
//            Email = request.EmailAddress,
//            FirstName = FirstName.Create(request.FirstName).Value,
//            LastName = LastName.Create(request.LastName).Value,
//            UserName = request.EmailAddress
//        };

//        var result = await userManager.CreateAsync(user, request.Password);

//        if (!result.Succeeded)
//        {
//            return Result.Failure<RegistrationResponse>(DomainErrors.ApplicationUser.UserRegistrationFailed);
//        }

//        var sendingResult = await accountEmailervice.SendConfirmationEmailAsync(user);

//        if (sendingResult.IsFailure)
//        {
//            return Result.Failure<RegistrationResponse>(sendingResult.Error);
//        }

//        return new RegistrationResponse(user.Id);
//    }
//    #endregion

//    #region Account confirmation
//    public async Task<Result> AccountConfirmationAsync(AccountConfirmationRequest request)
//    {
//        var user = (await userManager.ExistingUserAsync(request.EmailAddress))
//            .EmailAddressIsConfirmed(false);

//        if (user.IsFailure)
//        {
//            return Result.Failure(user.Error);
//        }

//        var decodedTokenBytes = WebEncoders.Base64UrlDecode(request.ConfirmationToken);

//        var decodedToken = Encoding.UTF8.GetString(decodedTokenBytes);

//        var result = await userManager.ConfirmEmailAsync(user.Value, decodedToken);

//        if (!result.Succeeded)
//        {
//            return Result.Failure(DomainErrors.ApplicationUser.AccountConfirmationFailed);
//        }

//        return Result.Success();
//    }

//    public async Task<Result> ResendConfirmationEmailAsync(EmailAddress emailAddress)
//    {
//        var user = (await userManager.ExistingUserAsync(emailAddress.Value))
//            .EmailAddressIsConfirmed(false);

//        if (user.IsFailure)
//        {
//            return Result.Failure(user.Error);
//        }

//        var result = await accountEmailervice.SendConfirmationEmailAsync(user.Value);

//        return result;
//    }
//    #endregion

//    #region RefreshToken
//    public async Task<Result<AuthenticationResponse>> RefreshTokenAsync(RefreshTokenRequest request)
//    {
//        var emailAddress = tokenProvider.GetUserEmailAddressFromExpiredToken(request.ExpiredAccessToken);

//        if (emailAddress.IsFailure)
//        {
//            return Result.Failure<AuthenticationResponse>(emailAddress.Error);
//        }

//        var user = await userManager.ExistingUserAsync(emailAddress.Value);

//        if (user.IsFailure)
//        {
//            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
//        }

//        if (user.Value.RefreshToken != request.RefreshToken || user.Value.IsRefrshTokenExpired)
//        {
//            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
//        }

//        var result = await userManager.IsLockedOutAsync(user.Value);

//        if (result)
//        {
//            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.AccountIsLocked(RemainingTime.Time(user.Value.LockoutEnd.Value)));
//        }

//        var refreshTokenResult = await tokenProvider.CreateRefreshTokenAsync(user.Value);

//        if (refreshTokenResult.IsFailure)
//        {
//            return Result.Failure<AuthenticationResponse>(refreshTokenResult.Error);
//        }

//        var response = new AuthenticationResponse
//        (
//            await tokenProvider.CreateAuthenticationTokenAsync(user.Value),
//            refreshTokenResult.Value.Token,
//            refreshTokenResult.Value.Expire
//        );

//        return response;
//    }
//    #endregion

//    #region Forgot password
//    public async Task<Result> ForgotPasswordAsync(EmailAddress emailAddress)
//    {
//        var user = (await userManager.ExistingUserAsync(emailAddress.Value))
//            .EmailAddressIsConfirmed(true);

//        if (user.IsFailure)
//        {
//            return Result.Failure(user.Error);
//        }

//        var resetToken = await userManager.GeneratePasswordResetTokenAsync(user.Value);

//        resetToken = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(resetToken));

//        var result = await accountEmailervice.SendResetPasswordEmailAsync(user.Value);

//        return result;
//    }

//    public async Task<Result> ResetPasswordAsync(ResetPasswordRequest request)
//    {
//        var user = (await userManager.ExistingUserAsync(request.EmailAddress))
//            .EmailAddressIsConfirmed(true);

//        if (user.IsFailure)
//        {
//            return Result.Failure(user.Error);
//        }

//        if (!request.Password.Equals(request.ConfirmPassword))
//        {
//            return Result.Failure(DomainErrors.ApplicationUser.ConfirmPasswordNotMatch);
//        }

//        var decodedTokenBytes = WebEncoders.Base64UrlDecode(request.ResetToken);

//        var decodedToken = Encoding.UTF8.GetString(decodedTokenBytes);

//        var result = await userManager.ResetPasswordAsync(user.Value, decodedToken, request.Password);

//        if (!result.Succeeded)
//        {
//            return Result.Failure(DomainErrors.ApplicationUser.PasswordResetFailed);
//        }

//        return Result.Success();
//    }
//    #endregion


//}
