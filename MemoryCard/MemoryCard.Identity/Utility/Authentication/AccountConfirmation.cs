﻿using MemoryCard.Application.Models.Identity.AccountConfirmation;
using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;

namespace MemoryCard.Identity.Utility.Authentication;

internal sealed class AccountConfirmation
{
    internal static async Task<Result> AccountConfirmationAsync(UserManager<ApplicationUser> userManager, AccountConfirmationRequest request)
    {
        var user = await userManager
            .EmailAddressIsConfirmedAsync(request.EmailAddress, false);

        if (user.IsFailure)
        {
            return Result.Failure(user.Error);
        }

        var decodedTokenBytes = WebEncoders.Base64UrlDecode(request.ConfirmationToken);

        var decodedToken = Encoding.UTF8.GetString(decodedTokenBytes);

        var result = await userManager.ConfirmEmailAsync(user.Value, decodedToken);

        if (!result.Succeeded)
        {
            return Result.Failure(DomainErrors.ApplicationUser.AccountConfirmationFailed);
        }

        return Result.Success();
    }
}
