﻿using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Constants;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace MemoryCard.Identity.Utility.Authentication;

internal sealed class Login
{
    internal static async Task<Result<AuthenticationResponse>> LoginAsync(
        UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager,
        ITokenProvider tokenProvider,
        AuthenticationRequest request)
    {
        var userResult = await userManager.EmailAddressIsConfirmedAsync(request.EmailAddress, true);

        if (userResult.IsFailure)
        {
            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
        }

        var user = userResult.Value;

        var result = await signInManager.PasswordSignInAsync(user.Email, request.Password, false, lockoutOnFailure: false);

        if (result.IsLockedOut)
        {
            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.AccountIsLocked(RemainingTime.Time(user.LockoutEnd.Value)));
        }

        if (!result.Succeeded)
        {
            if (!user.UserName.Equals(IdentityDataConstants.AdminUserName))
            {
                await userManager.AccessFailedAsync(user);
            }

            if (user.AccessFailedCount >= IdentityDataConstants.MaximumLoginAttempts)
            {
                await userManager.SetLockoutEndDateAsync(user, DateTime.UtcNow.AddMinutes(IdentityDataConstants.LockedPeriodInMinutes));
                return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.AccountIsLocked(RemainingTime.Time(user.LockoutEnd.Value)));
            }

            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
        }

        var authenticationToken = await tokenProvider.CreateAuthenticationTokenAsync(user);


        var refreshTokenResult = await tokenProvider.CreateRefreshTokenAsync(user);

        if (refreshTokenResult.IsFailure)
        {
            return Result.Failure<AuthenticationResponse>(refreshTokenResult.Error);
        }

        var response = new AuthenticationResponse
        (
            authenticationToken,
            refreshTokenResult.Value.Token,
            refreshTokenResult.Value.Expire
        );

        return response;
    }
}
