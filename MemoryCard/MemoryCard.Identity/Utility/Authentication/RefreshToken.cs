﻿using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Application.Models.Identity.RefreshToken;
using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace MemoryCard.Identity.Utility.Authentication;

internal class RefreshToken
{
    internal static async Task<Result<AuthenticationResponse>> RefreshTokenAsync(UserManager<ApplicationUser> userManager,ITokenProvider tokenProvider, RefreshTokenRequest request)
    {
        var emailAddress = tokenProvider.GetUserEmailAddressFromExpiredToken(request.ExpiredAccessToken);

        if (emailAddress.IsFailure)
        {
            return Result.Failure<AuthenticationResponse>(emailAddress.Error);
        }

        var user = await userManager.ExistingUserAsync(emailAddress.Value);

        if (user.IsFailure)
        {
            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
        }

        if (user.Value.RefreshToken != request.RefreshToken || user.Value.IsRefrshTokenExpired)
        {
            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.InvalidCredentials);
        }

        var result = await userManager.IsLockedOutAsync(user.Value);

        if (result)
        {
            return Result.Failure<AuthenticationResponse>(DomainErrors.ApplicationUser.AccountIsLocked(RemainingTime.Time(user.Value.LockoutEnd.Value)));
        }

        var refreshTokenResult = await tokenProvider.CreateRefreshTokenAsync(user.Value);

        if (refreshTokenResult.IsFailure)
        {
            return Result.Failure<AuthenticationResponse>(refreshTokenResult.Error);
        }

        var response = new AuthenticationResponse
        (
            await tokenProvider.CreateAuthenticationTokenAsync(user.Value),
            refreshTokenResult.Value.Token,
            refreshTokenResult.Value.Expire
        );

        return response;
    }
}
