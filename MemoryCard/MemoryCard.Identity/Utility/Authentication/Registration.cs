﻿using MemoryCard.Application.Models.Identity.Registration;
using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Shared;
using MemoryCard.Domain.ValueObjects;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace MemoryCard.Identity.Utility.Authentication;

internal sealed class Registration
{
    internal static async Task<Result<RegistrationResponse>> RegistrationAsync(
        UserManager<ApplicationUser> userManager,
        IAccountEmailService accountEmailService, 
        RegistrationRequest request)
    {
        var existingUser = await userManager.NoExistingUserAsync(request.EmailAddress);

        if (existingUser.IsFailure)
        {
            return Result.Failure<RegistrationResponse>(existingUser.Error);
        }

        if (!request.Password.Equals(request.ConfirmPassword))
        {
            return Result.Failure<RegistrationResponse>(DomainErrors.ApplicationUser.ConfirmPasswordNotMatch);
        }

        var user = new ApplicationUser
        {
            Email = request.EmailAddress,
            FirstName = FirstName.Create(request.FirstName).Value,
            LastName = LastName.Create(request.LastName).Value,
            UserName = request.EmailAddress
        };

        var result = await userManager.CreateAsync(user, request.Password);

        if (!result.Succeeded)
        {
            return Result.Failure<RegistrationResponse>(DomainErrors.ApplicationUser.UserRegistrationFailed);
        }

        var sendingResult = await accountEmailService.SendConfirmationEmailAsync(user);

        if (sendingResult.IsFailure)
        {
            return Result.Failure<RegistrationResponse>(sendingResult.Error);
        }

        return new RegistrationResponse(user.Id);
    }
}
