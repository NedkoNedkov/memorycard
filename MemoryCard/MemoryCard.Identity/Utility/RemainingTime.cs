﻿namespace MemoryCard.Identity.Utility;

internal static class RemainingTime
{
    public static TimeSpan Time(DateTimeOffset lockedEnd)
    {
        var difference = lockedEnd - DateTime.UtcNow;

        if (difference.TotalMinutes < 0)
        {
            return TimeSpan.Zero;
        }

        return difference;
    }
}
