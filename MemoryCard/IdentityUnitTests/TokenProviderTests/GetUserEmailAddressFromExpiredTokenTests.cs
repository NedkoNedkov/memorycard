﻿using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace IdentityUnitTests.TokenProviderTests;

public class GetUserEmailAddressFromExpiredTokenTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<IOptions<JWTSettings>> jwtSettings;

    private readonly ITokenProvider tokenProvider;

    public GetUserEmailAddressFromExpiredTokenTests()
    {
        userManager = UserManagerMock.UserManager;
        jwtSettings = IOptionsSettingsMock.JWTSettings;

        this.tokenProvider = new TokenProvider(userManager.Object, jwtSettings.Object);
    }

    [Fact]
    public void GetUserEmailAddressFromExpiredToken_IncorrectToken_Error_InvalidCredentials()
    {
        var expectedResult = DomainErrors.ApplicationUser.InvalidCredentials;

        var token = CreateFakeAuthenticationToken(TestEmail_Confirmed, SecurityAlgorithms.HmacSha512);

        var result = tokenProvider.GetUserEmailAddressFromExpiredToken(token);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public void GetUserEmailAddressFromExpiredToken_Success()
    {
        var expectedResult = TestEmail_Success;

        var token = CreateFakeAuthenticationToken(TestEmail_Success, SecurityAlgorithms.HmacSha256);

        var result = tokenProvider.GetUserEmailAddressFromExpiredToken(token);

        result.Value.ShouldBe(expectedResult);
    }

    private string CreateFakeAuthenticationToken(string emailAddress, string algorithms)
    {
        var claims = new[]
        {
            new Claim(JwtRegisteredClaimNames.Sub, TestFirstName),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new Claim(JwtRegisteredClaimNames.Email, emailAddress),
            new Claim("FirstName",TestFirstName),
            new Claim("LastName",TestLastName)
        };

        var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Object.Value.Key));
        var signinCredentials = new SigningCredentials(symetricSecurityKey, algorithms);
        var authenticationToken = new JwtSecurityToken(
            issuer: jwtSettings.Object.Value.Issuer,
            audience: jwtSettings.Object.Value.Audience,
            claims: claims,
            expires: DateTime.UtcNow.AddMinutes(jwtSettings.Object.Value.DurationInMinutes),
            signingCredentials: signinCredentials);

        return new JwtSecurityTokenHandler().WriteToken(authenticationToken);
    }
}
