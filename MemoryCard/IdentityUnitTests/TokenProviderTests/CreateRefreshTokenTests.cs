﻿using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;
using Microsoft.Extensions.Options;

namespace IdentityUnitTests.TokenProviderTests;

public class CreateRefreshTokenTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<IOptions<JWTSettings>> jwtSettings;

    private readonly ITokenProvider tokenProvider;

    public CreateRefreshTokenTests()
    {
        userManager = UserManagerMock.UserManager;
        jwtSettings = IOptionsSettingsMock.JWTSettings;

        this.tokenProvider = new TokenProvider(userManager.Object, jwtSettings.Object);
    }

    [Fact]
    public async Task CreateRefreshToken_Failed_Error_InvalidCredentials()
    {
        var expectedResult = DomainErrors.ApplicationUser.InvalidCredentials;

        var result = await tokenProvider.CreateRefreshTokenAsync(Users[TestEmail_CreateRefreshToken]);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task CreateRefreshToken_Success()
    {
        var expectedResult = true;

        var result = await tokenProvider.CreateRefreshTokenAsync(Users[TestEmail_CreateRefreshTokenSuccess]);

        result.IsSuccess.ShouldBe(expectedResult);
    }
}
