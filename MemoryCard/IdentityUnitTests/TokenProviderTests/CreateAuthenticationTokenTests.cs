﻿using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace IdentityUnitTests.TokenProviderTests;

public class CreateAuthenticationTokenTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<IOptions<JWTSettings>> jwtSettings;

    private readonly ITokenProvider tokenProvider;

    public CreateAuthenticationTokenTests()
    {
        userManager = UserManagerMock.UserManager;
        jwtSettings = IOptionsSettingsMock.JWTSettings;

        this.tokenProvider = new TokenProvider(userManager.Object,jwtSettings.Object);
    }

    [Fact]
    public async Task Create_AuthenticationToken_Expect_Token()
    {
        var expectedResult = TestEmail_Confirmed;

        var token = await tokenProvider.CreateAuthenticationTokenAsync(Users[expectedResult]);

        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            ValidateIssuer = true,
            ValidateAudience = false,
            ValidateLifetime = true,
            ClockSkew = TimeSpan.Zero,
            ValidIssuer = jwtSettings.Object.Value.Issuer,
            ValidAudience = jwtSettings.Object.Value.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Object.Value.Key))
        };

        SecurityToken securityToken;

        var principal = new JwtSecurityTokenHandler().ValidateToken(token, tokenValidationParameters, out securityToken);

        var result = principal?.FindFirst(ClaimTypes.Email)?.Value;

        result.ShouldBe(expectedResult);
    }
}
