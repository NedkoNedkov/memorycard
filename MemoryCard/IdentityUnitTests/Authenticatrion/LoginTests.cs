﻿using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Authenticatrion;

public class LoginTests
{
    private readonly Error ExpectedInvalidCredentials = DomainErrors.ApplicationUser.InvalidCredentials;

    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public LoginTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task Login_NoUser_Error_InvalidCredentials()
    {
        var request = new AuthenticationRequest(TestEmail_NoUser, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task Login_EmailNoConfirmed_Error_InvalidCredentials()
    {
        var request = new AuthenticationRequest(TestEmail_Exist_ExpiredRefreshToken, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task Login_UserLocked_Error_AccountIsLocked()
    {
        var expectedResult = DomainErrors.ApplicationUser.AccountIsLocked(new TimeSpan(0, 1, 1)).Code;

        var request = new AuthenticationRequest(TestEmail_LockedUser, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Error.Code.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task Login_Verify_UserManger_AccessFailedAssync_Once()
    {
        var request = new AuthenticationRequest(TestEmail_SignIn_Failed, TestPassword);

        _ = await authenticationService.LoginAsync(request);

        userManager.Verify(x => x.AccessFailedAsync(It.IsAny<ApplicationUser>()), Times.Once);
    }

    [Fact]
    public async Task Login_Reache_MaxLoginAttempts_Error_AccountIsLocked()
    {
        var expectedResult = DomainErrors.ApplicationUser.AccountIsLocked(new TimeSpan(0, 1, 1)).Code;

        var request = new AuthenticationRequest(TestEmail_MaxAccessFailed, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Error.Code.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task Handle_AccessFailed_Error_InvalidCredentials()
    {
        var request = new AuthenticationRequest(TestEmail_SignIn_Failed, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task Handle_CreateRefreshToken_Failed_Error_InvalidCredentials()
    {
        var request = new AuthenticationRequest(TestEmail_Confirmed, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task Login_Success()
    {
        var expectedResult = TestAccessToken;

        var request = new AuthenticationRequest(TestEmail_Success, TestPassword);

        var result = await authenticationService.LoginAsync(request);

        result.Value.AuthenticationToken.ShouldBe(expectedResult);
    }
}
