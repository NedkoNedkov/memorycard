﻿using MemoryCard.Domain.ValueObjects;
using MemoryCard.Identity.Contracts;
using Microsoft.AspNetCore.Identity;

namespace IdentityUnitTests.Authenticatrion;

public class ForgotPasswordTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public ForgotPasswordTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task ForgotPassword_NoUser_Error_NotFound()
    {
        var expectedResult = DomainErrors.ApplicationUser.NotFound(TestEmail_NoUser);

        var request = EmailAddress.Create(TestEmail_NoUser).Value;

        var result = await authenticationService.ForgotPasswordAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ForgotPassword_EmailAddress_NoConfirmed_Error_EmailAddressNotConfirm()
    {
        var expectedResult = DomainErrors.ApplicationUser.EmailAddressNotConfirm;

        var request = EmailAddress.Create(TestEmail_Exist_ExpiredRefreshToken).Value;

        var result = await authenticationService.ForgotPasswordAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ForgotPassword_SendResetpasswordToken_Failed_Error_SendEmailFailed()
    {
        var expectedResult = DomainErrors.EmailService.SendEmailFailed;

        var request = EmailAddress.Create(TestEmail_Confirmed).Value;

        var result = await authenticationService.ForgotPasswordAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ForgotPassword_SendResetpasswordToken_Success()
    {
        var request = EmailAddress.Create(TestEmail_Success).Value;

        var result = await authenticationService.ForgotPasswordAsync(request);

        result.IsSuccess.ShouldBeTrue();
    }
}
