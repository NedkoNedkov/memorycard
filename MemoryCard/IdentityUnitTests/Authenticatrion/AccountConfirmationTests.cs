﻿using MemoryCard.Application.Models.Identity.AccountConfirmation;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Authenticatrion;

public class AccountConfirmationTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public AccountConfirmationTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task AccountConfirmation_UserNotFound_Error_NotFound()
    {
        var expectedResult = DomainErrors.ApplicationUser.NotFound(TestEmail_NoUser);

        var request = new AccountConfirmationRequest(TestEmail_NoUser, string.Empty);

        var result = await authenticationService.AccountConfirmationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task AccountConfirmation_AccountConfirmed_Error_EmailAlreadyInUse()
    {
        var expectedResult = DomainErrors.ApplicationUser.EmailAlreadyInUse;

        var request = new AccountConfirmationRequest(TestEmail_Confirmed, string.Empty);

        var result = await authenticationService.AccountConfirmationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task AccountConfirmation_InvalidConfirmationToken_Error_AccountConfirmationFailed()
    {
        var expectedResult = DomainErrors.ApplicationUser.AccountConfirmationFailed;

        var request = new AccountConfirmationRequest(TestEmail_Invalid_ConfirmationToken, string.Empty);

        var result = await authenticationService.AccountConfirmationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task AccountConfirmation_Confirm_Account_Success()
    {
        var request = new AccountConfirmationRequest(TestEmail_Exist_ExpiredRefreshToken, string.Empty);

        var result = await authenticationService.AccountConfirmationAsync(request);

        result.IsSuccess.ShouldBeTrue();
    }
}
