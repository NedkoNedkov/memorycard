﻿using MemoryCard.Application.Models.Identity.Registration;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Authenticatrion;

public class RegistrationTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public RegistrationTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task Registration_UserExist_Error_EmailAlreadyInUse()
    {
        var expectedResult = DomainErrors.ApplicationUser.EmailAlreadyInUse;

        var request = new RegistrationRequest(TestFirstName, TestLastName, TestEmail_Exist_ExpiredRefreshToken, TestPassword, TestPassword);

        var result = await authenticationService.RegistrationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task Registration_ConfirmPasswordNotMatch_Error_ConfirmPasswordNotMatch()
    {
        var expectedResult = DomainErrors.ApplicationUser.ConfirmPasswordNotMatch;

        var request = new RegistrationRequest(TestFirstName, TestLastName, TestEmail_NoUser, TestPassword, TestWrongPassword);

        var result = await authenticationService.RegistrationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task Registration_UserManeger_CanNotCreateUser_Error_UserRegistrationFailed()
    {
        var expectedResult = DomainErrors.ApplicationUser.UserRegistrationFailed;

        var request = new RegistrationRequest(TestFirstName, TestLastName, TestEmail_NoUser, TestWrongPassword, TestWrongPassword);

        var result = await authenticationService.RegistrationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task Registration_SendConfirmationEmailFailed_Error_SendEmailFailed()
    {
        var expectedResult = DomainErrors.EmailService.SendEmailFailed;

        var request = new RegistrationRequest(TestFirstName, TestLastName, TestEmail_NoUser, TestPassword, TestPassword);

        var result = await authenticationService.RegistrationAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task Registration_Registration_Successful()
    {
        var request = new RegistrationRequest(TestFirstName, TestLastName, TestEmail_RegistrationSuccess, TestPassword, TestPassword);

        var result = await authenticationService.RegistrationAsync(request);

        result.IsSuccess.ShouldBeTrue();
    }
}
