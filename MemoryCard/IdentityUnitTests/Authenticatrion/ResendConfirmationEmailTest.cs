﻿using MemoryCard.Domain.ValueObjects;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Authenticatrion;

public class ResendConfirmationEmailTest
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public ResendConfirmationEmailTest()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task ResentCinfirmationEamil_NoUser_Error_NotFound()
    {
        var expectedResult = DomainErrors.ApplicationUser.NotFound(TestEmail_NoUser);

        var request = EmailAddress.Create(TestEmail_NoUser).Value;

        var result = await authenticationService.ResendConfirmationEmailAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ResentCinfirmationEamil_AccountConfirmed_Error_EmailAlreadyInUse()
    {
        var expectedResult = DomainErrors.ApplicationUser.EmailAlreadyInUse;

        var request = EmailAddress.Create(TestEmail_Confirmed).Value;

        var result = await authenticationService.ResendConfirmationEmailAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ResentCinfirmationEamil_Success()
    {
        var request = EmailAddress.Create(TestEmail_ResendEmail_Success).Value;

        var result = await authenticationService.ResendConfirmationEmailAsync(request);

        result.IsSuccess.ShouldBeTrue();
    }
}
