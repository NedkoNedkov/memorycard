﻿using MemoryCard.Application.Models.Identity.RefreshToken;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Authenticatrion;

public class RefreshTokenTests
{
    private readonly Error ExpectedInvalidCredentials = DomainErrors.ApplicationUser.InvalidCredentials;

    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public RefreshTokenTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task RefreshToken_GetEmailAddress_FromExpiredToken_Failure_Error_InvalidCredentials()
    {
        var request = new RefreshTokenRequest(Test_Wrong_ExpiredAccessToken, TestRefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task RefreshToken_UserNotFound_Error_InvalidCredentials()
    {
        var request = new RefreshTokenRequest(TestAccessToken_Wrong_Email, TestRefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task RefreshToken_RefreshtokenEpired_Error_InvalidCredentials()
    {
        var request = new RefreshTokenRequest(TestAccessToken_ExpiredRefresh, TestExpired_RefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task RefreshToken_RefreshtokenDontMatch_Error_InvalidCredentials()
    {
        var request = new RefreshTokenRequest(TestAccessToken_Wrong_RefreshToken, TestRefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task RefreshToken_UserIsLocked_Error_AccountIsLocked()
    {
        var expectedResult = DomainErrors.ApplicationUser.AccountIsLocked(new TimeSpan(0, 1, 1)).Code;

        var request = new RefreshTokenRequest(TestAccessToken_UserLocked, TestRefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        result.Error.Code.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task RefreshToken_TokenProvider_CanNotCreate_RefreshToken_Error_InvalidCredentials()
    {
        var request = new RefreshTokenRequest(TestAccessToken_ConfimedEmail, TestRefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        result.Error.ShouldBe(ExpectedInvalidCredentials);
    }

    [Fact]
    public async Task RefreshToken_Success()
    {
        var ecpectetResult = TestAccessToken + TestRefreshToken;

        var request = new RefreshTokenRequest(TestAccessToken_EmailSuccess, TestRefreshToken);

        var result = await authenticationService.RefreshTokenAsync(request);

        ecpectetResult.ShouldBe(result.Value.AuthenticationToken + result.Value.RefreshToken);
    }
}
