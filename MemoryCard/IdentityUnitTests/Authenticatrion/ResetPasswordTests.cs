﻿using MemoryCard.Application.Models.Identity.ResetPassword;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Authenticatrion;

public class ResetPasswordTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<SignInManager<ApplicationUser>> signInManager;
    private readonly Mock<ITokenProvider> tokenProvider;
    private readonly Mock<IAccountEmailService> accountEmailService;

    private readonly IAuthenticationService authenticationService;

    public ResetPasswordTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.signInManager = SignInManagerMock.SignInManager;
        this.tokenProvider = ITokenProviderMock.TokenProvider;
        this.accountEmailService = IAccountEmailServiceMock.AccountEmailService;
        this.authenticationService = new AuthenticationService(userManager.Object, signInManager.Object, tokenProvider.Object, accountEmailService.Object);
    }

    [Fact]
    public async Task ResetPassword_UserNotFound_Error_ApplicationUserNotFound()
    {
        var expectedResult = DomainErrors.ApplicationUser.NotFound(TestEmail_NoUser);

        var request = new ResetPasswordRequest(TestEmail_NoUser, string.Empty, TestPassword, TestPassword);

        var result = await authenticationService.ResetPasswordAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ResetPassword_UserEmail_NotConfirm_Error_EmailAddressNotConfirm()
    {
        var expectedResult = DomainErrors.ApplicationUser.EmailAddressNotConfirm;

        var request = new ResetPasswordRequest(TestEmail_Exist_ExpiredRefreshToken, string.Empty, TestPassword, TestPassword);

        var result = await authenticationService.ResetPasswordAsync (request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ResetPassword_ConfirmPassword_NotMatch_Error_ConfirmPasswordNotMatch()
    {
        var expectedResult = DomainErrors.ApplicationUser.ConfirmPasswordNotMatch;

        var request = new ResetPasswordRequest(TestEmail_Confirmed, string.Empty, TestPassword, TestWrongPassword);

        var result = await authenticationService.ResetPasswordAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ResetPassword_UserManegerResetPassword_Failure_Error_PasswordResetFailed()
    {
        var expectedResult = DomainErrors.ApplicationUser.PasswordResetFailed;

        var request = new ResetPasswordRequest(TestEmail_Confirmed, string.Empty, TestWrongPassword, TestWrongPassword);

        var result = await authenticationService.ResetPasswordAsync(request);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task ResetPassword_ResetPassword_Successfull()
    {
        var request = new ResetPasswordRequest(TestEmail_Confirmed, string.Empty, TestPassword, TestPassword);

        var result = await authenticationService.ResetPasswordAsync(request);

        result.IsSuccess.ShouldBeTrue();
    }
}
