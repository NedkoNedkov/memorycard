﻿using MemoryCard.Application.Contracts.Infrastructure;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;
using Microsoft.Extensions.Options;

namespace IdentityUnitTests.AccountEmailServiceTests;

public class SendResetPasswordTokenTests
{
    private readonly Mock<UserManager<ApplicationUser>> userManager;
    private readonly Mock<IEmailService> emailService;
    private readonly Mock<IOptions<AuthenticationEmailSettings>> emailSettings;
    private readonly Mock<IOptions<JWTSettings>> jwtSettings;

    private readonly IAccountEmailService accountEmailService;

    public SendResetPasswordTokenTests()
    {
        this.userManager = UserManagerMock.UserManager;
        this.emailService = IEmailServiceMock.EmailService;
        this.emailSettings = IOptionsSettingsMock.AuthenticationEmailSettings;
        this.jwtSettings = IOptionsSettingsMock.JWTSettings;
        this.accountEmailService = new AccountEmailService(userManager.Object, emailService.Object, jwtSettings.Object, emailSettings.Object);
    }

    [Fact]
    public async Task SendResetPassowrdToken_EmailNotConfirmed_Error_EmailAddressNotConfirm()
    {
        var expectedResult = DomainErrors.ApplicationUser.EmailAddressNotConfirm;

        var result = await accountEmailService.SendResetPasswordEmailAsync(Users[TestEmail_Exist_ExpiredRefreshToken]);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task SendResetPassowrdToken_SendEmail_Failed_Error_SendEmailFailed()
    {
        var expectedResult = DomainErrors.EmailService.SendEmailFailed;

        var result = await accountEmailService.SendResetPasswordEmailAsync(Users[TestEmail_Confirmed]);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task SendResetPassowrdToken_Success()
    {
        var expectedResult = true;

        var result = await accountEmailService.SendResetPasswordEmailAsync(Users[TestEmail_Success]);

        result.IsSuccess.ShouldBe(expectedResult);
    }
}
