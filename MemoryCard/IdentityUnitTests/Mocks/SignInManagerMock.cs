﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace IdentityUnitTests.Mocks;

public class SignInManagerMock
{
    public static Mock<SignInManager<ApplicationUser>> SignInManager
    {
        get
        {
            var signInManager = new Mock<SignInManager<ApplicationUser>>(UserManagerMock.UserManager.Object,
                Mock.Of<IHttpContextAccessor>(),
                Mock.Of<IUserClaimsPrincipalFactory<ApplicationUser>>(),
                null, null, null, null);

            signInManager.Setup(s => s.PasswordSignInAsync(It.Is<string>(x => x == TestEmail_LockedUser), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.LockedOut);

            signInManager.Setup(s => s.PasswordSignInAsync(It.Is<string>(x => x == TestEmail_SignIn_Failed), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Failed);

            signInManager.Setup(s => s.PasswordSignInAsync(It.Is<string>(x => x == TestEmail_MaxAccessFailed), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Failed);

            signInManager.Setup(s => s.PasswordSignInAsync(It.Is<string>(x => x == TestEmail_Confirmed), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Success);

            signInManager.Setup(s => s.PasswordSignInAsync(It.Is<string>(x => x == TestEmail_Success), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Success);

            return signInManager;
        }
    }
}
