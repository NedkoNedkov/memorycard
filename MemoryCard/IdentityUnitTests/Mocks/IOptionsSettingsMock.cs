﻿using MemoryCard.Identity.Models;
using Microsoft.Extensions.Options;

namespace IdentityUnitTests.Mocks;

public class IOptionsSettingsMock
{
    public const string Url = "localhost";
    public const string BackPath = "localhost/path";

    public static Mock<IOptions<JWTSettings>> JWTSettings
    {
        get
        {
            var mockTokenSettings = new Mock<IOptions<JWTSettings>>();

            mockTokenSettings.Setup(s => s.Value)
                .Returns(new JWTSettings
                {
                    Key = "03RCzu/LT/48EeliJG9L/ZS/ITwGAYUUoALJSePkG5k=03RCzu/LT/48EeliJG9L/ZS/ITwGAYUUoALJSePkG5k=",
                    Issuer = Url,
                    Audience = Url,
                    AppUrl = Url,
                    DurationInMinutes = 15,
                    RefreshTokenDurationInHours = 1
                });

            return mockTokenSettings;
        }
    }

    public static Mock<IOptions<AuthenticationEmailSettings>> AuthenticationEmailSettings
    {
        get
        {
            var mockEmailSettings = new Mock<IOptions<AuthenticationEmailSettings>>();

            mockEmailSettings.Setup(e => e.Value)
                .Returns(new AuthenticationEmailSettings
                {
                    From = TestEmail_Success,
                    ApplicationName = AplicationName,
                    ConfirmEmailPath = BackPath,
                    ResetPasswordPath = BackPath
                });

            return mockEmailSettings;
        }
    }
}
