﻿using MemoryCard.Domain.Shared;
using MemoryCard.Domain.ValueObjects;
using MemoryCard.Identity.Contracts;

namespace IdentityUnitTests.Mocks;

public class IAccountEmailServiceMock
{
    private static Result success = Result.Success();
    private static Result failure = Result.Failure(DomainErrors.EmailService.SendEmailFailed);

    public static Mock<IAccountEmailService> AccountEmailService
    {
        get
        {         
            var mockAccountEmailService = new Mock<IAccountEmailService>();

            SetUpSendConfirmationEmailAsync(mockAccountEmailService);

            return mockAccountEmailService;
        }
    }

    private static void SetUpSendConfirmationEmailAsync(Mock<IAccountEmailService> mockAccountEmailService)
    {
        mockAccountEmailService.Setup(e => e.SendConfirmationEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_ResendEmail_Success)))
            .ReturnsAsync(success);

        mockAccountEmailService.Setup(e => e.SendConfirmationEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_NoUser)))
                .ReturnsAsync(failure);

        mockAccountEmailService.Setup(e => e.SendConfirmationEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_RegistrationSuccess)))
            .ReturnsAsync(success);

        mockAccountEmailService.Setup(e => e.SendResetPasswordEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Confirmed)))
            .ReturnsAsync(failure);

        mockAccountEmailService.Setup(e => e.SendResetPasswordEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Success)))
            .ReturnsAsync(success);
    }
}
