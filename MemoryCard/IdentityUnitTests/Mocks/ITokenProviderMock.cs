﻿using MemoryCard.Domain.Shared;
using MemoryCard.Identity.Contracts;
using MemoryCard.Identity.Models;

namespace IdentityUnitTests.Mocks;

public class ITokenProviderMock
{
    public const int RefreshTokenLifeInHours = 1;

    public static Mock<ITokenProvider> TokenProvider
    {
        get
        {
            var tokenProvider = new Mock<ITokenProvider>();
            SetUpCreateRefreshToken(tokenProvider);
            SetUpCreateAuthenticationTokenAsync(tokenProvider);
            SetUpGetUserEmailAddressFromExpiredToken(tokenProvider);
            return tokenProvider;
        }
    }

    private static void SetUpCreateRefreshToken(Mock<ITokenProvider> tokenProvider)
    {
        var refreshToken = new RefreshToken(TestRefreshToken, DateTime.UtcNow.AddHours(RefreshTokenLifeInHours));

        tokenProvider.Setup(t => t.CreateRefreshTokenAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Confirmed)))
            .ReturnsAsync(Result.Failure<RefreshToken>(DomainErrors.ApplicationUser.InvalidCredentials));

        tokenProvider.Setup(t => t.CreateRefreshTokenAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Success)))
            .ReturnsAsync(refreshToken);
    }

    private static void SetUpGetUserEmailAddressFromExpiredToken(Mock<ITokenProvider> tokenProvider)
    {
        var invalidCredentials = Result.Failure<string>(DomainErrors.ApplicationUser.InvalidCredentials);
        var credentialsSuccessful = Result.Success(TestRefreshToken);

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == Test_Wrong_ExpiredAccessToken)))
            .Returns(invalidCredentials);

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken)))
            .Returns(credentialsSuccessful);

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken_Wrong_Email)))
            .Returns(Result.Success(TestEmail_NoUser));

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken_ExpiredRefresh)))
            .Returns(Result.Success(TestEmail_Exist_ExpiredRefreshToken));

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken_Wrong_RefreshToken)))
            .Returns(Result.Success(TestEmail_Wrong_RefreshToken));

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken_UserLocked)))
            .Returns(Result.Success(TestEmail_LockedUser));

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken_ConfimedEmail)))
            .Returns(TestEmail_Confirmed);

        tokenProvider.Setup(t => t.GetUserEmailAddressFromExpiredToken(It.Is<string>(x => x == TestAccessToken_EmailSuccess)))
            .Returns(TestEmail_Success);
    }

    private static void SetUpCreateAuthenticationTokenAsync(Mock<ITokenProvider> tokenProvider)
    {
        tokenProvider.Setup(t => t.CreateAuthenticationTokenAsync(It.IsAny<ApplicationUser>()))
            .ReturnsAsync(TestAccessToken);
    }
}
