﻿using MemoryCard.Identity.Constants;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace IdentityUnitTests.Mocks;

public class UserManagerMock
{
    private static readonly IdentityResult IdentityResultFailure = IdentityResult.Failed();
    private static readonly IdentityResult IdentityResultSuccess = IdentityResult.Success;

    public static Mock<UserManager<ApplicationUser>> UserManager
    {
        get
        {
            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            var mockUserManager = new Mock<UserManager<ApplicationUser>>(mockUserStore.Object, null, null, null, null, null, null, null, null);

            SetUpCreateAsync(mockUserManager);
            SetUpFindByEmailAsinc(mockUserManager);
            SetUpResetPasswordAsync(mockUserManager);
            SetUpIsLockedOutAsync(mockUserManager);
            SetUpConfirmEmailAsync(mockUserManager);
            SetUpGeneratePasswordResetTokenAsync(mockUserManager);
            SetUpGenerateEmailConfirmationTokenAsync(mockUserManager);
            SetUpGetClaimsAsync(mockUserManager);
            SetUpGetRolesAsync(mockUserManager);
            SetUpUpdateAsync(mockUserManager);

            return mockUserManager;
        }
    }

    private static void SetUpUpdateAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.UpdateAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_CreateRefreshToken)))
           .ReturnsAsync(IdentityResult.Failed());

        mockUserManager.Setup(m => m.UpdateAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_CreateRefreshTokenSuccess)))
           .ReturnsAsync(IdentityResult.Success);
    }

    private static void SetUpGetRolesAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.GetRolesAsync(It.IsAny<ApplicationUser>()))
           .ReturnsAsync(new List<string> { IdentityDataConstants.AdminRole });
    }

    private static void SetUpGetClaimsAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.GetClaimsAsync(It.IsAny<ApplicationUser>()))
           .ReturnsAsync(new List<Claim>());
    }

    private static void SetUpGenerateEmailConfirmationTokenAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.GenerateEmailConfirmationTokenAsync(It.IsAny<ApplicationUser>()))
            .ReturnsAsync(TestAccessToken);
    }

    private static void SetUpCreateAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.CreateAsync(It.IsAny<ApplicationUser>(), It.Is<string>(x => x == TestPassword)))
            .ReturnsAsync(IdentityResultSuccess);

        mockUserManager.Setup(m => m.CreateAsync(It.IsAny<ApplicationUser>(), It.Is<string>(x => x == TestWrongPassword)))
            .ReturnsAsync(IdentityResultFailure);
    }

    private static void SetUpFindByEmailAsinc(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_ResendEmail_Success)))
            .ReturnsAsync(Users[TestEmail_ResendEmail_Success]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_Exist_ExpiredRefreshToken)))
            .ReturnsAsync(Users[TestEmail_Exist_ExpiredRefreshToken]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_Confirmed)))
            .ReturnsAsync(Users[TestEmail_Confirmed]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_Wrong_RefreshToken)))
            .ReturnsAsync(Users[TestEmail_Wrong_RefreshToken]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_LockedUser)))
            .ReturnsAsync(Users[TestEmail_LockedUser]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_Success)))
            .ReturnsAsync(Users[TestEmail_Success]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_AccountConfirm_Success)))
           .ReturnsAsync(Users[TestEmail_Exist_ExpiredRefreshToken]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_Invalid_ConfirmationToken)))
            .ReturnsAsync(Users[TestEmail_Invalid_ConfirmationToken]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_SignIn_Failed)))
            .ReturnsAsync(Users[TestEmail_SignIn_Failed]);

        mockUserManager.Setup(m => m.FindByEmailAsync(It.Is<string>(x => x == TestEmail_MaxAccessFailed)))
            .ReturnsAsync(Users[TestEmail_MaxAccessFailed]);
    }

    private static void SetUpResetPasswordAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.Is<string>(x => x == TestWrongPassword)))
            .ReturnsAsync(IdentityResultFailure);

        mockUserManager.Setup(m => m.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.Is<string>(x => x == TestPassword)))
           .ReturnsAsync(IdentityResultSuccess);
    }

    private static void SetUpIsLockedOutAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.IsLockedOutAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_LockedUser)))
            .ReturnsAsync(true);

        mockUserManager.Setup(m => m.IsLockedOutAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Confirmed)))
            .ReturnsAsync(false);

        mockUserManager.Setup(m => m.IsLockedOutAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Success)))
            .ReturnsAsync(false);
    }

    private static void SetUpConfirmEmailAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.ConfirmEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Invalid_ConfirmationToken), It.IsAny<string>()))
            .ReturnsAsync(IdentityResultFailure);

        mockUserManager.Setup(m => m.ConfirmEmailAsync(It.Is<ApplicationUser>(x => x.Email == TestEmail_Exist_ExpiredRefreshToken), It.IsAny<string>()))
            .ReturnsAsync(IdentityResultSuccess);
    }

    private static void SetUpGeneratePasswordResetTokenAsync(Mock<UserManager<ApplicationUser>> mockUserManager)
    {
        mockUserManager.Setup(m => m.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
            .ReturnsAsync(TestAccessToken);
    }
}
