﻿using MemoryCard.Application.Contracts.Infrastructure;
using MemoryCard.Application.Models.Mail;

namespace IdentityUnitTests.Mocks;

public class IEmailServiceMock
{
    public static Mock<IEmailService> EmailService
    {
        get
        {
            var mockSEmailSrvice = new Mock<IEmailService>();

            mockSEmailSrvice.Setup(e => e.SendEmailAsync(It.Is<EmailForm>(x => x.Recipient.First().EmailAddress.Value == TestEmail_Confirmed)))
                .ReturnsAsync(Result.Failure(DomainErrors.EmailService.SendEmailFailed));

            mockSEmailSrvice.Setup(e => e.SendEmailAsync(It.Is<EmailForm>(x => x.Recipient.First().EmailAddress.Value == TestEmail_Success)))
                .ReturnsAsync(Result.Success);

            return mockSEmailSrvice;
        }
    }
}
