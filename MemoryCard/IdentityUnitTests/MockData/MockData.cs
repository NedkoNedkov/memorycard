﻿using MemoryCard.Domain.ValueObjects;
using MemoryCard.Identity.Constants;
using MemoryCard.Identity.Models.Entities;

namespace IdentityUnitTests.MockData;

public class MockData
{
    public const string TestEmail_NoUser = "nouser@test.com";
    public const string TestEmail_Confirmed = "confirm@test.com";
    public const string TestEmail_Success = "success.email@test.com";
    public const string TestEmail_Wrong_RefreshToken = "badrefreshtoken@test.com";
    public const string TestEmail_Exist_ExpiredRefreshToken = "exist@test.com";
    public const string TestEmail_LockedUser = "locked@test.com";
    public const string TestEmail_RegistrationSuccess = "registration.success@test.com";
    public const string TestEmail_AccountConfirm_Success = "confirmaccount@test.com";
    public const string TestEmail_Invalid_ConfirmationToken = "wrongconfirmtoken@test.com";
    public const string TestEmail_SignIn_Failed = "failedSignIn@test.com";
    public const string TestEmail_MaxAccessFailed = "maxaccessfailed@test.com";
    public const string TestEmail_ResendEmail_Success = "resndemailsuccess@test.com";
    public const string TestEmail_CreateRefreshToken = "createrefreshtoken@test.com";
    public const string TestEmail_CreateRefreshTokenSuccess = "createrefreshtokensuccess@test.com";

    public const string TestFirstName = "TestFirst";
    public const string TestLastName = "TestLast";
    public const string TestPassword = "password";
    public const string TestWrongPassword = "wrong_password";

    public const string TestRefreshToken = "refresh_token";
    public const string TestExpired_RefreshToken = "expired_refresh_token";
    public const string TestWrong_RefreshToken = "TestWrong_RefreshToken";
    public const string TestAccessToken = "access_token";
    public const string TestAccessToken_ConfimedEmail = "TestAccessToken_ConfimedEmail";
    public const string TestAccessToken_EmailSuccess = "TestAccessToken_EmailSuccess";
    public const string Test_Wrong_ExpiredAccessToken = "wrong_expiredaccesstoken";
    public const string TestAccessToken_Wrong_Email = "wrong_email_accesstoken";
    public const string TestAccessToken_Wrong_RefreshToken = "refreshtoken_dont_match";
    public const string TestAccessToken_ExpiredRefresh = "TestAccessToken_ExpiredRefresh";
    public const string TestAccessToken_UserLocked = "TestAccessToken_UserLocked";

    public const string AplicationName = "MemoryCards";

    public static readonly Dictionary<string, ApplicationUser> Users = new Dictionary<string, ApplicationUser>(
        new List<KeyValuePair<string, ApplicationUser>>
                {
                    new (TestEmail_Exist_ExpiredRefreshToken,
                        new ApplicationUser {
                            Email = TestEmail_Exist_ExpiredRefreshToken,
                            EmailConfirmed = false,
                            RefreshToken = TestExpired_RefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddMinutes(-10) } ),
                    new (TestEmail_ResendEmail_Success,
                        new ApplicationUser {
                            Email = TestEmail_ResendEmail_Success,
                            EmailConfirmed = false,
                            RefreshToken = TestExpired_RefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddMinutes(-10) } ),
                    new(TestEmail_Confirmed,
                        new ApplicationUser() {
                            Email = TestEmail_Confirmed,
                            EmailConfirmed = true,
                            RefreshToken = TestRefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddDays(10)}),
                    new(TestEmail_CreateRefreshToken,
                        new ApplicationUser() {
                            Email = TestEmail_CreateRefreshToken,
                            EmailConfirmed = true,
                            RefreshToken = TestRefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddDays(10)}),
                    new(TestEmail_CreateRefreshTokenSuccess,
                        new ApplicationUser() {
                            Email = TestEmail_CreateRefreshTokenSuccess,
                            EmailConfirmed = true,
                            RefreshToken = TestRefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddDays(10)}),
                    new(TestEmail_Wrong_RefreshToken,
                        new ApplicationUser() {Email = TestEmail_Wrong_RefreshToken,
                            RefreshToken = TestWrong_RefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddDays(10)}),
                    new(TestEmail_LockedUser,
                        new ApplicationUser() {
                            Email = TestEmail_LockedUser,
                            EmailConfirmed = true,
                            RefreshToken = TestRefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddDays(10),
                            LockoutEnd = DateTimeOffset.UtcNow.AddMinutes(30)}),
                    new(TestEmail_Success,
                        new ApplicationUser() {
                            Email = TestEmail_Success,
                            EmailConfirmed = true,
                            RefreshToken = TestRefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddDays(10)}),
                    new (TestEmail_Invalid_ConfirmationToken,
                        new ApplicationUser {
                            Email = TestEmail_Invalid_ConfirmationToken,
                            EmailConfirmed = false,
                            RefreshToken = TestExpired_RefreshToken,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            RefreshTokenExpireTime = DateTime.UtcNow.AddMinutes(-10) } ),
                    new(TestEmail_SignIn_Failed,
                        new ApplicationUser() {
                            Email = TestEmail_SignIn_Failed,
                            EmailConfirmed = true,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            UserName = TestEmail_SignIn_Failed}),
                    new(TestEmail_MaxAccessFailed,
                        new ApplicationUser() {
                            Email = TestEmail_MaxAccessFailed,
                            EmailConfirmed = true,
                            UserName = TestEmail_MaxAccessFailed,
                            FirstName = FirstName.Create(TestFirstName).Value,
                            LastName = LastName.Create(TestLastName).Value,
                            AccessFailedCount = IdentityDataConstants.MaximumLoginAttempts,
                            LockoutEnd = DateTimeOffset.UtcNow.AddMinutes(10)})
                });
}
