global using Xunit;
global using MemoryCard.Identity.Models.Entities;
global using MemoryCard.Domain.Errors;
global using Microsoft.AspNetCore.Identity;
global using MemoryCard.Identity.Services;
global using IdentityUnitTests.Mocks;
global using MemoryCard.Domain.Shared;
global using MemoryCard.Application.Contracts.Identity;
global using Moq;
global using Shouldly;

global using static IdentityUnitTests.MockData.MockData;