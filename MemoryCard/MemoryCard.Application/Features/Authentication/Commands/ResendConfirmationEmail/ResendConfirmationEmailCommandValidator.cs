﻿using FluentValidation;

namespace MemoryCard.Application.Features.Authentication.Commands.ResendConfirmationEmail;

internal class ResendConfirmationEmailCommandValidator : AbstractValidator<ResendConfirmationEmailCommand>
{
    public ResendConfirmationEmailCommandValidator()
    {
        RuleFor(x => x.EmailAddress)
            .NotEmpty()
            .NotNull()
            .EmailAddress();
    }
}
