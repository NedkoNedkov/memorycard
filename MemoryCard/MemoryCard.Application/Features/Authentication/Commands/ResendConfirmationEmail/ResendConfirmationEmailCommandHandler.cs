﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Domain.Shared;
using MemoryCard.Domain.ValueObjects;

namespace MemoryCard.Application.Features.Authentication.Commands.ResendConfirmationEmail;

public sealed class ResendConfirmationEmailCommandHandler : AuthenticationHandler, ICommandHandler<ResendConfirmationEmailCommand>
{
    public ResendConfirmationEmailCommandHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result> Handle(ResendConfirmationEmailCommand request, CancellationToken cancellationToken)
    {
        var resultRequest = EmailAddress.Create(request.EmailAddress).Value;

        var result = await authenticationService.ResendConfirmationEmailAsync(resultRequest);

        return result;
    }
}
