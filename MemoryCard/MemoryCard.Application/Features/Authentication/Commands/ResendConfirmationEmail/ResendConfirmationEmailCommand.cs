﻿using MemoryCard.Application.Contracts;

namespace MemoryCard.Application.Features.Authentication.Commands.ResendConfirmationEmail;

public sealed record ResendConfirmationEmailCommand(string EmailAddress) : ICommand;
