﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Features.Authentication.Commands;

public sealed class AutenticateCommandHandler : AuthenticationHandler, ICommandHandler<AuthenticationRequest, AuthenticationResponse>
{
    public AutenticateCommandHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result<AuthenticationResponse>> Handle(AuthenticationRequest request, CancellationToken cancellationToken)
    {
        var result = await authenticationService.LoginAsync(request);

        return result;
    }
}
