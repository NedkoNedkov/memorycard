﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Application.Models.Identity.ResetPassword;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Features.Authentication.Commands;

public sealed class ResetPasswordCommandHandler : AuthenticationHandler, ICommandHandler<ResetPasswordRequest>
{
    public ResetPasswordCommandHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result> Handle(ResetPasswordRequest request, CancellationToken cancellationToken)
    {
        var result = await authenticationService.ResetPasswordAsync(request);

        return result;
    }
}
