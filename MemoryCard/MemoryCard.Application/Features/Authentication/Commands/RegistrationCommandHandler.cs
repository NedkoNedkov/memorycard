﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Application.Models.Identity.Registration;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Features.Authentication.Commands;

public sealed class RegistrationCommandHandler : AuthenticationHandler, ICommandHandler<RegistrationRequest, RegistrationResponse>
{
    public RegistrationCommandHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result<RegistrationResponse>> Handle(RegistrationRequest request, CancellationToken cancellationToken)
    {
        var result = await authenticationService.RegistrationAsync(request);

        return result;
    }
}
