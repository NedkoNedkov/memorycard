﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Application.Models.Identity.AccountConfirmation;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Features.Authentication.Commands;

public sealed class AccountConfirmationHandler : AuthenticationHandler,  ICommandHandler<AccountConfirmationRequest>
{
    public AccountConfirmationHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result> Handle(AccountConfirmationRequest request, CancellationToken cancellationToken)
    {
        var result = await authenticationService.AccountConfirmationAsync(request);

        return result;
    }
}
