﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Domain.Shared;
using MemoryCard.Domain.ValueObjects;

namespace MemoryCard.Application.Features.Authentication.Commands.ForgotPassword;

public sealed class ForgotPasswordCommandHandler : AuthenticationHandler, ICommandHandler<ForgotPasswordCommand>
{
    public ForgotPasswordCommandHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result> Handle(ForgotPasswordCommand command, CancellationToken cancellationToken)
    {
        var request = EmailAddress.Create(command.EmailAddres).Value;

        var result = await authenticationService.ForgotPasswordAsync(request);

        return result;
    }
}
