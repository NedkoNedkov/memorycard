﻿using MemoryCard.Application.Contracts;

namespace MemoryCard.Application.Features.Authentication.Commands.ForgotPassword;

public sealed record ForgotPasswordCommand(string EmailAddres) : ICommand;
