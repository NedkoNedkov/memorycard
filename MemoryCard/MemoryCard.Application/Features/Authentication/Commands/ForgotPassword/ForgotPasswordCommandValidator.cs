﻿using FluentValidation;

namespace MemoryCard.Application.Features.Authentication.Commands.ForgotPassword;

internal class ForgotPasswordCommandValidator : AbstractValidator<ForgotPasswordCommand>
{
    public ForgotPasswordCommandValidator()
    {
        RuleFor(x => x.EmailAddres)
            .NotEmpty()
            .NotNull()
            .EmailAddress();
    }
}
