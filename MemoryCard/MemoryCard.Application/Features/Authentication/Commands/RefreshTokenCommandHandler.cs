﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;
using MemoryCard.Application.Features.Authentication.Abstraction;
using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Application.Models.Identity.RefreshToken;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Features.Authentication.Commands;

public sealed class RefreshTokenCommandHandler : AuthenticationHandler, ICommandHandler<RefreshTokenRequest, AuthenticationResponse>
{
    public RefreshTokenCommandHandler(IAuthenticationService authenticationService) : base(authenticationService)
    {
    }

    public async Task<Result<AuthenticationResponse>> Handle(RefreshTokenRequest request, CancellationToken cancellationToken)
    {
        var result = await authenticationService.RefreshTokenAsync(request);

        return result;
    }
}
