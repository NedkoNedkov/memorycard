﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Contracts.Identity;

namespace MemoryCard.Application.Features.Authentication.Abstraction;

public abstract class AuthenticationHandler
{
    protected readonly IAuthenticationService authenticationService;

    public AuthenticationHandler(IAuthenticationService authenticationService) => this.authenticationService = authenticationService;

}
