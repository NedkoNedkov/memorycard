﻿using MemoryCard.Application.Models.Mail;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Contracts.Infrastructure;

public interface IEmailService
{
    public Task<Result> SendEmailAsync(EmailForm email);
}
