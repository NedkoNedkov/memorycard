﻿using MemoryCard.Application.Models.Identity.AccountConfirmation;
using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Application.Models.Identity.ResetPassword;
using MemoryCard.Application.Models.Identity.RefreshToken;
using MemoryCard.Application.Models.Identity.Registration;
using MemoryCard.Domain.Shared;
using MemoryCard.Domain.ValueObjects;

namespace MemoryCard.Application.Contracts.Identity;

public interface IAuthenticationService
{
    public Task<Result<AuthenticationResponse>> LoginAsync(AuthenticationRequest request);
    public Task<Result<RegistrationResponse>> RegistrationAsync(RegistrationRequest request);
    public Task<Result> AccountConfirmationAsync(AccountConfirmationRequest request);
    public Task<Result> ResendConfirmationEmailAsync(EmailAddress emailAddress);
    public Task<Result<AuthenticationResponse>> RefreshTokenAsync(RefreshTokenRequest request);
    public Task<Result> ForgotPasswordAsync(EmailAddress emailAddress);
    public Task<Result> ResetPasswordAsync(ResetPasswordRequest request);
}
