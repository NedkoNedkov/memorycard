﻿namespace MemoryCard.Application.Contracts;

public interface ILoggedInUserService
{
    public string EmailAddress { get; }
}
