﻿using MediatR;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Contracts;

public interface ICommand : IRequest<Result>
{
}

public interface ICommand<TResponse> : IRequest<Result<TResponse>>
{
}
