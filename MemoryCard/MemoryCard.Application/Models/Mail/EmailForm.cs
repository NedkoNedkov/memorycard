﻿using MemoryCard.Application.Models.UserModels;

namespace MemoryCard.Application.Models.Mail;

public class EmailForm
{
    public EmailForm(RegularMember sender, List<RegularMember> recipient, string? subject, string emailBody, bool isBodyHTML = true)
    {
        Sender = sender;
        Recipient = recipient;
        Subject = subject;
        EmailBody = emailBody;
        IsBodyHTML = isBodyHTML;
    }

    public RegularMember Sender { get; private set; }
    public List<RegularMember> Recipient { get; private set; }
    public string? Subject { get; private set; }
    public string EmailBody { get; private set; }
    public bool IsBodyHTML { get; private set; }
}
