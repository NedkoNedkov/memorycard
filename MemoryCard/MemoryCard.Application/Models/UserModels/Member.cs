﻿using MemoryCard.Domain.ValueObjects;

namespace MemoryCard.Application.Models.UserModels;

public sealed class Member : RegularMember
{
    public Member(EmailAddress emailAddress, string firstName, string lastName) : base(emailAddress, $"{firstName} {lastName}")
    {
        this.FirstName = FirstName.Create(firstName).Value;
        this.LastName = LastName.Create(lastName).Value;
    }

    public FirstName FirstName { get; }
    public LastName LastName { get; }
}
