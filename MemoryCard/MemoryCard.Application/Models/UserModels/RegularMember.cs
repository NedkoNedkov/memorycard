﻿using MemoryCard.Domain.ValueObjects;

namespace MemoryCard.Application.Models.UserModels;

public class RegularMember
{
    public RegularMember(EmailAddress emailAddress, string fullName)
    {
        EmailAddress = emailAddress;
        FullName = fullName;
    }

    public EmailAddress EmailAddress { get; }

    public string FullName { get; }

    protected virtual string FillToString() => string.Empty;
}
