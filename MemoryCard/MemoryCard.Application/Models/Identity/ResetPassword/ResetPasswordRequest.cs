﻿using MemoryCard.Application.Contracts;

namespace MemoryCard.Application.Models.Identity.ResetPassword;

public sealed record ResetPasswordRequest(
    string EmailAddress,
    string ResetToken,
    string Password,
    string ConfirmPassword) : ICommand;
