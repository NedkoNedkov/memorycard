﻿using MediatR;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Application.Models.Identity;

public record EmailAddressRequest(string EmailAddress);
