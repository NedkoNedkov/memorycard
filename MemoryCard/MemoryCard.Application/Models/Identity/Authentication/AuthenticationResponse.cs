﻿namespace MemoryCard.Application.Models.Identity.Authentication;

public record AuthenticationResponse(string AuthenticationToken, string RefreshToken, DateTime RefreshTokenExpirationTime);
