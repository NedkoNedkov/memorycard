﻿using FluentValidation;

namespace MemoryCard.Application.Models.Identity.Authentication;

public class AuthenticationRequestValidator : AbstractValidator<AuthenticationRequest>
{
    public AuthenticationRequestValidator()
    {
        RuleFor(p => p.EmailAddress)
            .NotEmpty()
            .NotNull()
            .EmailAddress();

        RuleFor(p => p.Password)
            .NotEmpty()
            .NotNull();
    }
}
