﻿using FluentValidation;
using MemoryCard.Application.Contracts;

namespace MemoryCard.Application.Models.Identity.Authentication;

public record AuthenticationRequest(string EmailAddress, string Password) : ICommand<AuthenticationResponse>;