﻿using FluentValidation;

namespace MemoryCard.Application.Models.Identity.AccountConfirmation;

public class AccountConfirmationRequestValidator : AbstractValidator<AccountConfirmationRequest>
{
    public AccountConfirmationRequestValidator()
    {
        RuleFor(p => p.EmailAddress)
           .NotEmpty()
           .NotNull()
           .EmailAddress();

        RuleFor(p => p.ConfirmationToken)
           .NotEmpty()
           .NotNull();
    }
}
