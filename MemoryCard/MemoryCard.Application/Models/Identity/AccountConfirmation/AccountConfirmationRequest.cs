﻿using MemoryCard.Application.Contracts;

namespace MemoryCard.Application.Models.Identity.AccountConfirmation;

public record AccountConfirmationRequest(string EmailAddress, string ConfirmationToken) : ICommand;
