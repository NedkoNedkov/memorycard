﻿using FluentValidation;
using MemoryCard.Domain.ValueObjects;

namespace MemoryCard.Application.Models.Identity.Registration;

internal sealed class RegistrationRequestValidator : AbstractValidator<RegistrationRequest>
{
    public RegistrationRequestValidator()
    {
        RuleFor(p => p.FirstName)
            .NotEmpty()
            .NotNull()
            .MaximumLength(FirstName.MaxLength);

        RuleFor(p => p.LastName)
            .NotEmpty()
            .NotNull()
            .MaximumLength(LastName.MaxLength);

        RuleFor(p => p.EmailAddress)
            .NotEmpty()
            .NotNull()
            .EmailAddress();

        RuleFor(p => p.Password)
            .NotEmpty()
            .NotNull()
            .MinimumLength(8);

        RuleFor(p => p.ConfirmPassword)
            .NotEmpty()
            .NotNull();
    }
}
