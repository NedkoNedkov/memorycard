﻿using MemoryCard.Application.Contracts;

namespace MemoryCard.Application.Models.Identity.Registration;

public sealed record RegistrationRequest(
    string FirstName,
    string LastName,
    string EmailAddress,
    string Password,
    string ConfirmPassword) : ICommand<RegistrationResponse>;