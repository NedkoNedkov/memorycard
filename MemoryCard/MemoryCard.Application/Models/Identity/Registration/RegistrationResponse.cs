﻿namespace MemoryCard.Application.Models.Identity.Registration;

public record RegistrationResponse(string UserId);
