﻿using FluentValidation;

namespace MemoryCard.Application.Models.Identity.RefreshToken;

internal sealed class RefreshTokenRequestValidator : AbstractValidator<RefreshTokenRequest>
{
    public RefreshTokenRequestValidator()
    {
        RuleFor(p => p.ExpiredAccessToken)
            .NotEmpty()
            .NotNull();

        RuleFor(p => p.RefreshToken)
            .NotEmpty()
            .NotNull();
    }
}
