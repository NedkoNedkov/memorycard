﻿using MemoryCard.Application.Contracts;
using MemoryCard.Application.Models.Identity.Authentication;

namespace MemoryCard.Application.Models.Identity.RefreshToken;

public sealed record RefreshTokenRequest(string ExpiredAccessToken, string RefreshToken) : ICommand<AuthenticationResponse>;
