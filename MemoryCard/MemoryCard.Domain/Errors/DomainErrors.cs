﻿using MemoryCard.Domain.Shared;

namespace MemoryCard.Domain.Errors;

public static class DomainErrors
{
    public static class Email
    {
        public static readonly Error Empty = new(
            "Email.Empty",
            "Email is empty");

        public static readonly Error TooLong = new(
            "Email.TooLong",
            "Email is too long");

        public static readonly Error InvalidFormat = new(
            "Email.InvalidFormat",
            "Email format is invalid");
    }

    public static class FirstName
    {
        public static readonly Error Empty = new(
            "FirstName.Empty",
            "First name is empty");

        public static readonly Error TooLong = new(
            "FirstName.Long",
            "First name is too long");
    }

    public static class LastName
    {
        public static readonly Error Empty = new(
            "LastName.Empty",
            "Last name is empty");

        public static readonly Error TooLong = new(
            "LastName.TooLong",
            "Last name is too long");
    }

    public static class EmailService
    {
        public static Error SendEmailFailed = new(
            "AccountEmailService.SendEmailFailed",
            "Failed to send email. PLease contact admin");
    }

    public static class ApplicationUser
    {
        public static readonly Error EmailAlreadyInUse = new(
            "ApplicationUser.EmailAlreadyInUse",
            "The specified email is already in use");

        public static readonly Func<string, Error> NotFound = id => new Error(
            "ApplicationUser.NotFound",
            $"The member with the identifier {id} was not found.");

        public static readonly Error InvalidCredentials = new(
            "ApplicationUser.InvalidCredentials",
            "The provided credentials are invalid");

        public static readonly Error EmailAddressNotConfirm = new(
            "ApplicationUser.EmailAddressNotConfirm",
            "Please confrim your email address");

        public static readonly Func<TimeSpan, Error> AccountIsLocked = time => new Error(
            "ApplicationUser.AccountIsLocked",
            $"Your account has been locked. You should be able to login in {(int)time.TotalMinutes} minutes and {time.Seconds} seconds.");

        public static readonly Error ConfirmPasswordNotMatch = new(
            "ApplicationUser.ConfirmPasswordNotMatch",
            "ConfirmPassword not match");

        public static readonly Error UserRegistrationFailed = new(
            "ApplicationUser.UserRegistrationFailed",
            "User registration failed");

        public static readonly Error AccountConfirmationFailed = new(
            "ApplicationUser.UserRegistrationFailed",
            "Account confirmation failed");

        public static readonly Error PasswordResetFailed = new(
            "ApplicationUser.PasswordResetFailed",
            "Password reset failed");
    }
}
