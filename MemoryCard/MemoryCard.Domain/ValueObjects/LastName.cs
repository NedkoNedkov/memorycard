﻿using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Primitives;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Domain.ValueObjects;

public class LastName : ValueObject
{
    public const int MaxLength = 50;

    private LastName(string value) => Value = value;

    private LastName()
    {
    }

    public string Value { get; private set; }

    public static Result<LastName> Create(string lastName) =>
        Result.Create(lastName)
            .Ensure(ln => !string.IsNullOrWhiteSpace(ln), DomainErrors.LastName.Empty)
            .Ensure(ln => ln.Length <= MaxLength, DomainErrors.LastName.TooLong)
            .Map(ln => new LastName(ln));

    protected override IEnumerable<object> GetAtomivValues()
    {
        yield return Value;
    }
}
