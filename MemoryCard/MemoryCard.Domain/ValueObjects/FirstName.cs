﻿using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Primitives;
using MemoryCard.Domain.Shared;

namespace MemoryCard.Domain.ValueObjects;

public class FirstName : ValueObject
{
    public const int MaxLength = 50;

    private FirstName(string value) => Value = value;

    private FirstName() 
    {
    }

    public string Value { get; private set; }

    public static Result<FirstName> Create(string firstName) =>
        Result.Create(firstName)
            .Ensure(fn => !string.IsNullOrWhiteSpace(fn),DomainErrors.FirstName.Empty)
            .Ensure(fn => fn.Length <= MaxLength,DomainErrors.LastName.TooLong)
            .Map(fn => new FirstName(fn));

    protected override IEnumerable<object> GetAtomivValues()
    {
        yield return Value;
    }
}
