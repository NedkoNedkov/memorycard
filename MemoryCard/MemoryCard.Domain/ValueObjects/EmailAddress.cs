﻿using MemoryCard.Domain.Errors;
using MemoryCard.Domain.Primitives;
using MemoryCard.Domain.Shared;
using System.Text.RegularExpressions;

namespace MemoryCard.Domain.ValueObjects;

public sealed class EmailAddress : ValueObject
{
    public const string EmailRegexTemplate = @"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$";
    public const int MaxLength = 255;

    private static readonly Regex regex = new Regex(EmailRegexTemplate);

    private EmailAddress(string value) => Value = value; 

    private EmailAddress() 
    {
    }

    public string Value { get; private set; }

    public static Result<EmailAddress> Create(string email) =>
        Result.Create(email)
            .Ensure(e => !string.IsNullOrWhiteSpace(e), DomainErrors.Email.Empty)
            .Ensure(e => e.Length <= MaxLength, DomainErrors.Email.TooLong)
            .Ensure(e => regex.IsMatch(e), DomainErrors.Email.InvalidFormat)
            .Map(e => new EmailAddress(e));

    protected override IEnumerable<object> GetAtomivValues()
    {
        yield return Value;
    }
}
