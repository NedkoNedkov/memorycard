﻿namespace MemoryCard.Domain.Shared;

public class Result<TValue> : Result
{
    private readonly TValue value;

    protected internal Result(TValue value, bool isSuccess, Error error)
        :base(isSuccess, error) => this.value = value;

    public TValue Value => IsSuccess ? this.value : throw new InvalidOperationException(string.Format(Error.Message));

    public static implicit operator Result<TValue>(TValue? value) => Create(value);
}
