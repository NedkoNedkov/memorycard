﻿namespace MemoryCard.Domain.Primitives;

public abstract class ValueObject : IEquatable<ValueObject>
{
    public static bool operator ==(ValueObject one, ValueObject two)
    {
        return EqualOperator(one, two);
    }

    public static bool operator !=(ValueObject one, ValueObject two)
    {
        return NotEqualOperator(one, two);
    }

    protected static bool EqualOperator (ValueObject left, ValueObject right) => left is not null && right is not null && left.Equals(right);

    protected static bool NotEqualOperator(ValueObject left, object right) => !(left == right);

    protected abstract IEnumerable<object> GetAtomivValues();

    public bool Equals(ValueObject? other) => other is not null && ValuesAreEquals(other);

    public override bool Equals(object obj) => obj is not null && obj is ValueObject other && ValuesAreEquals(other);

    public override int GetHashCode() => GetAtomivValues().Aggregate(default(int), HashCode.Combine);

    private bool ValuesAreEquals(ValueObject other) => GetAtomivValues().SequenceEqual(other.GetAtomivValues());
}
