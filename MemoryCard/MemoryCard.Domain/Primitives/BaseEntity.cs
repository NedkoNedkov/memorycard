﻿using System;

namespace MemoryCard.Domain.Primitives;

public abstract class BaseEntity : IEquatable<BaseEntity>
{
    protected BaseEntity() 
    {
        this.Id = Guid.NewGuid().ToString();
    }

    public string Id { get; private init; }

    public static bool operator == (BaseEntity? left, BaseEntity? right) => left is not null && right is not null && left.Equals(right);

    public static bool operator !=(BaseEntity? left, BaseEntity right) => !(left == right);

    public bool Equals(BaseEntity? other)
    {
        if(other is null)
        {
            return false;
        }

        if(other.GetType() != GetType()) 
        {
            return false;
        }

        return other.Id == Id;
    }

    public override bool Equals(object? obj)
    {
        if(obj is null)
        {
            return false;
        }

        if(obj.GetType() != GetType())
        {
            return false;
        }

        if(obj is not BaseEntity entity)
        {
            return false;
        }

        return entity.Id == Id;
    }

    public override int GetHashCode() => Id.GetHashCode()*41;
}
