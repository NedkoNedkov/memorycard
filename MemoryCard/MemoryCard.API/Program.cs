using Serilog;

using MemoryCard.API;


        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateBootstrapLogger();

        Log.Information("Memory Cards API starting");

        var builder = WebApplication.CreateBuilder(args);

        builder.Host.UseSerilog((context, loggerConfiguration) => loggerConfiguration
        .WriteTo.Console()
        .ReadFrom.Configuration(context.Configuration));

        var app = builder
            .ConfigureServices()
            .ConfigurePipeLine();

        app.UseSerilogRequestLogging();           

        app.Run();

public partial class Program {}
