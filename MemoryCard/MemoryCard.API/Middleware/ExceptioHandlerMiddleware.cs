﻿using System.Net;
using System.Text.Json;

namespace MemoryCard.API.Middleware;

public class ExceptioHandlerMiddleware
{
    private readonly RequestDelegate next;

    public ExceptioHandlerMiddleware(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await this.next(context);
        }
        catch (Exception ex)
        {
            await ConvertException(context,ex);
        }
    }

    private Task ConvertException(HttpContext context, Exception exception)
    {
        HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;

        context.Response.ContentType = "application/json";

        context.Response.StatusCode = (int)httpStatusCode;

        var result = JsonSerializer.Serialize(new { error = exception.Message });

        return context.Response.WriteAsync(result);
    }
}
