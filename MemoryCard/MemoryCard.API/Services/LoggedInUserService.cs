﻿using MemoryCard.Application.Contracts;
using System.Security.Claims;

namespace MemoryCard.API.Services;

public class LoggedInUserService : ILoggedInUserService
{
    private readonly IHttpContextAccessor contextAccessor;

    public LoggedInUserService(IHttpContextAccessor contextAccessor)
    {
        this.contextAccessor = contextAccessor;
    }

    public string EmailAddress
    {
        get
        {
            return this.contextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.Email)?.Value;
        }
    }
}
