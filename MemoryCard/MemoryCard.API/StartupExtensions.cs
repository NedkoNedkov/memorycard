﻿using MemoryCard.API.Utility;
using Microsoft.OpenApi.Models;
using MemoryCard.Application;
using MemoryCard.Infrastructure;
using MemoryCard.Identity;
using MemoryCard.API.Middleware;
using FluentValidation;
using System.Reflection;
using MemoryCard.Application.Contracts;
using MemoryCard.API.Services;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using MemoryCard.Domain.Shared;
using Microsoft.IdentityModel.Tokens;
using Org.BouncyCastle.Asn1.Ocsp;

namespace MemoryCard.API;

public static class StartupExtensions
{
    public static WebApplication ConfigureServices(this WebApplicationBuilder builder)
    {
        AddSwagger(builder.Services);

        builder.Services.AddScoped<ILoggedInUserService, LoggedInUserService>();

        builder.Services.AddApplicationServices();
        builder.Services.AddInfrastructureService(builder.Configuration);
        builder.Services.AddIdentityServices(builder.Configuration);

        builder.Services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly(), includeInternalTypes: true);

        builder.Services.AddHttpContextAccessor();

        builder.Services.AddControllers();

        builder.Services.AddCors(option =>
        {
            option.AddPolicy("Open", builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
        });

        return builder.Build();
    }

    public static WebApplication ConfigurePipeLine(this WebApplication app)
    {
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Memory Cards API");
            });
        }
        app.UseHttpsRedirection();

        app.UseAuthentication();

        app.UseCustomExceptionHandler();

        app.UseCors("Open");

        app.UseAuthorization();

        app.MapControllers();

        return app;
    }

    private static void AddSwagger(IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = "Bearer authentication with JWT token.",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
                Scheme = "Bearer",
                BearerFormat = "JWT"
            });

            c.AddSecurityRequirement(new OpenApiSecurityRequirement()
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }//,
                        //Scheme = "oauth2",
                        //Name = "Bearer",
                        //In = ParameterLocation.Header
                    },
                    new List<string>()
                }
            });

            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "Memory Cards API"
            });

            c.OperationFilter<FileResultContentTypeOperationFilter>();
        });
    }
}
