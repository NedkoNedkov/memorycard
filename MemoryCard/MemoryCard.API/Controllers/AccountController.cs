﻿using MediatR;
using MemoryCard.API.Abstractions;
using MemoryCard.Application.Features.Authentication.Commands.ForgotPassword;
using MemoryCard.Application.Models.Identity;
using MemoryCard.Application.Models.Identity.AccountConfirmation;
using MemoryCard.Application.Models.Identity.Authentication;
using MemoryCard.Application.Models.Identity.ResetPassword;
using MemoryCard.Application.Models.Identity.RefreshToken;
using MemoryCard.Application.Models.Identity.Registration;
using Microsoft.AspNetCore.Mvc;
using MemoryCard.Application.Features.Authentication.Commands.ResendConfirmationEmail;

namespace MemoryCard.API.Controllers;

[Route("api/[controller]")]
public class AccountController : ApiController
{
    public const string RefreshTokenCookeName = "MemoryCardsRefreshToken";

    public AccountController(ISender sender) : base(sender)
    {
    }

    [HttpPost("login")]
    public async Task<IActionResult> AuthenticateAsync([FromBody]AuthenticationRequest request, CancellationToken cancellationToken)
    {
        var result = await sender.Send(request, cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        var response = result.Value.AuthenticationToken;

        this.SaveRefreshToken(result.Value.RefreshToken, result.Value.RefreshTokenExpirationTime);

        return Ok(response);
    }

    [HttpPost("register")]
    public async Task<IActionResult> RegisterAsync(RegistrationRequest request, CancellationToken cancellationToken)
    {
        var result = await sender.Send(request, cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return Ok(result.Value);
    }

    [HttpPut("confirm-account")]
    public async Task<IActionResult> AccountConfirmation(AccountConfirmationRequest request, CancellationToken cancellationToken)
    {
        var result = await sender.Send(request, cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return Ok();
    }

    [HttpPost("forgot-password")]
    public async Task<IActionResult> ForgotPasswordAsync(EmailAddressRequest request, CancellationToken cancellationToken)
    {
        var command = new ForgotPasswordCommand(request.EmailAddress);

        var result = await sender.Send(command, cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return Ok();
    }

    [HttpPut("reset-password")]
    public async Task<IActionResult> ResetPasswordAsync(ResetPasswordRequest request, CancellationToken cancellationToken)
    {
        var result = await sender.Send(request, cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return Ok();
    }

    [HttpPut("resend-confirmation-email")]
    public async Task<IActionResult> ResendConfirmationEmailAsync(EmailAddressRequest request)
    {
        var command = new ResendConfirmationEmailCommand(request.EmailAddress);

        var result = await sender.Send(command);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return Ok();
    }

    [HttpPost("refresh-token")]
    public async Task<IActionResult> RefreshToken([FromHeader] string ExpiredToken)
    {
        var token = Request.Cookies[RefreshTokenCookeName];

        var request = new RefreshTokenRequest(ExpiredToken, token);

        var result = await sender.Send(request);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        this.SaveRefreshToken(result.Value.RefreshToken, result.Value.RefreshTokenExpirationTime);

        return Ok(result.Value.AuthenticationToken);
    }

    private void SaveRefreshToken(string refreshToken, DateTime expire)
    {
        var cookieOptions = new CookieOptions
        {
            Expires = expire,
            IsEssential = true,
            HttpOnly = true,
        };

        Response.Cookies.Append(RefreshTokenCookeName, refreshToken, cookieOptions);
    }
}
