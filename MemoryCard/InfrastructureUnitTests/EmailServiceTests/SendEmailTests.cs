﻿using MemoryCard.Application.Contracts.Infrastructure;
using MemoryCard.Application.Models.Mail;
using MemoryCard.Application.Models.UserModels;
using MemoryCard.Domain.Errors;
using MemoryCard.Domain.ValueObjects;
using MemoryCard.Infrastructure.Mail;
using MemoryCard.Infrastructure.Mail.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Shouldly;

namespace InfrastructureUnitTests.EmailServiceTests;
public class SendEmailTests
{
    public readonly EmailAddress TestEmail = EmailAddress.Create("test@test.com").Value;

    private IConfiguration configurations;
    private readonly IEmailService emailService;

    public SendEmailTests()
    {
        configurations = new ConfigurationBuilder()
            .AddUserSecrets<SendEmailTests>()
            .Build();

        EmailSettings emailSettings = new EmailSettings()
        {
            From = configurations.GetSection("EmailSettings").GetValue<string>("From"),
            Password = configurations.GetSection("EmailSettings").GetValue<string>("Password"),
            Port = configurations.GetSection("EmailSettings").GetValue<int>("Port"),
            SmtpServer = configurations.GetSection("EmailSettings").GetValue<string>("SmtpServer"),
            UserName = configurations.GetSection("EmailSettings").GetValue<string>("UserName")
        };

        Mock<IOptions<EmailSettings>> settings = new Mock<IOptions<EmailSettings>>();
        settings.Setup(x => x.Value)
            .Returns(emailSettings);

        Mock<ILogger<EmailService>> logger = new Mock<ILogger<EmailService>>();

        emailService = new EmailService(settings.Object, logger.Object);
    }

    [Fact]
    public async Task SendEmail_Failed_Error_SendEmailFailed()
    {
        var expectedResult = DomainErrors.EmailService.SendEmailFailed;

        var form = new EmailForm(
            new RegularMember(TestEmail, ""),
            new List<RegularMember> { null },
            "",
            "");

        var result = await emailService.SendEmailAsync(form);

        result.Error.ShouldBe(expectedResult);
    }

    [Fact]
    public async Task SendEmail_Success()
    {
        var form = new EmailForm(
            new RegularMember(TestEmail, TestEmail.Value),
            new List<RegularMember> { new RegularMember(TestEmail, TestEmail.Value) },
            "",
            "");

        var result = await emailService.SendEmailAsync(form);

        result.IsSuccess.ShouldBeTrue();
    }
}
