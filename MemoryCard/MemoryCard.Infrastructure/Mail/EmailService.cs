﻿using MemoryCard.Application.Contracts.Infrastructure;
using MemoryCard.Application.Models.Mail;
using MailKit.Net.Smtp;
using MimeKit;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MemoryCard.Domain.Shared;
using MemoryCard.Infrastructure.Mail.Models;
using MemoryCard.Domain.Errors;

namespace MemoryCard.Infrastructure.Mail;

public class EmailService : IEmailService
{
    private const string SuccessSentMessage = "Email sent";

    private readonly EmailSettings emailSettings;
    private readonly ILogger<EmailService> logger;

    public EmailService(IOptions<EmailSettings> emailSettings, ILogger<EmailService> logger)
    {
        this.emailSettings = emailSettings.Value;
        this.logger = logger;
    }

    public async Task<Result> SendEmailAsync(EmailForm email)
    {
        MimeMessage message;

        using (var client = new SmtpClient())
        {
            try
            {
                message = CreateMessage(email);
                await client.ConnectAsync(this.emailSettings.SmtpServer, this.emailSettings.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(this.emailSettings.UserName, this.emailSettings.Password);
                var result = await client.SendAsync(message);
            }
            catch
            {
                var result = Result.Failure(DomainErrors.EmailService.SendEmailFailed);
                this.logger.LogError(result.Error.Message);
                return result;
            }
            finally
            {
                await client.DisconnectAsync(true);
                client.Dispose();
            }
        }

        this.logger.LogInformation(SuccessSentMessage);
        return Result.Success();
    }

    private MimeMessage CreateMessage(EmailForm email)
    {
        var messege = new MimeMessage();

        messege.To.AddRange(email.Recipient.Select(r => new MailboxAddress(r.FullName, r.EmailAddress.Value)));

        messege.From.Add(new MailboxAddress(email.Sender.FullName, email.Sender.EmailAddress.Value));

        messege.Subject = email.Subject;

        messege.Body = email.IsBodyHTML ?
            new TextPart(MimeKit.Text.TextFormat.Html) { Text = email.EmailBody }:
            new TextPart(MimeKit.Text.TextFormat.Plain) { Text = email.EmailBody };

        return messege;
    }
}
